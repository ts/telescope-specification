import { defineConfig } from 'vite'
import { resolve } from 'path'
import react from '@vitejs/plugin-react'
import dts from 'vite-plugin-dts'
import * as packageJson from "./package.json";

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
    }
  },
  plugins: [react(),dts({
    insertTypesEntry: true, 
      include: ['lib'] ,
       
    
  })],
    build: {
        rollupOptions: {
          external: Object.keys(packageJson.peerDependencies),
           },
      copyPublicDir:false,
        lib: {
          entry: resolve('lib/main.js'),
          formats: ["es", "cjs"],
        fileName: (format) =>
        `telescope-specification.${format === "cjs" ? "cjs" : "es.js"}`,
    
        },
      minify: false  
      }
})




import './App.css'
import "@fortawesome/fontawesome-free/css/all.css";
import {SpecificationDetailsForm} from '../lib/main'
import constraintTemplates from './data/constraintTemplates.json'
import strategies from './data/strategies.json'
import taskFilters from './data/taskFilters.json'
import taskTemplates from './data/taskTemplates.json'
import commonSchemaTemplate from './data/commonSchemaTemplate.json'
//import adefinition from "./data/definition.scincalation.json"

import { useState } from 'react';
function App() {

  const handleStrategyChange = (event) => {
    let selectedStrategyUrl =event.target.value;
    let newlySelectedStrategy=strategies?.find((strategy) => strategy.url === selectedStrategyUrl) 
    setDefinition(undefined)
    setCount(count+1);
    setSelectedStrategy(newlySelectedStrategy);
    console.log("newlySelectedStrategy",newlySelectedStrategy)
  };

  const [selectedStrategy, setSelectedStrategy] = useState(strategies[0]); // adefinition.observation_strategy_template ||
  const [definition, setDefinition] = useState();
  const [constraintSchema, setConstraintSchema] = useState();
  const [status, setStatus] = useState();
  const [count, setCount] = useState(1);
  const disabled=false
  // add to form to get paramsoutput
  const paramsOutput={ 
    "Duration FE1": 200
}
  const definitionCallback = (definition, newStatus) => {
    setDefinition(definition);
    console.log("my definition is now",definition)
    setStatus(newStatus);
  }

  const constraintSchemaCallback = (constraintSchema) => {
    setConstraintSchema(constraintSchema);
  }
  
  

  return (
    <div>

    <div>
       <select id="strategy select" value={selectedStrategy?.url} onChange={handleStrategyChange}>
        <option value="">--Please choose an strategy--</option>
        {strategies.map(option => (
          <option key={option.id} value={option.url}>
            {`${option.name} - v${option.version}`}
          </option>
        ))}
        </select>
        </div>
        <h3> Selected Strategy </h3>
         <table style={{border:"1px",width:"100%"}}>
            <tbody>
            <tr>
              <td style={{verticalAlign:"top"}}>
              <div>Params Output</div>
                      <pre style={{maxWidth:"900px",height:"calc(20h - 80px)",textAlign:"left",backgroundColor:"#FFDDFF",overflow:"scroll"}}>{JSON.stringify(paramsOutput,null,1)}</pre> 
        <div>Status Output</div>

                              <pre>{JSON.stringify(status,null,1)}</pre>
              <div>ConstraintSchema / ref_resolved_schema.sky.transit_offset.to = </div>  
                  <pre style={{maxWidth:"900px",height:"calc(23vh - 130px)",textAlign:"left",backgroundColor:"#FFDDEE",overflow:"scroll"}}>{JSON.stringify(constraintSchema?.ref_resolved_schema.properties.sky.properties.transit_offset.properties.to,null,1)}</pre>
                      <pre style={{maxWidth:"900px",height:"calc(23vh - 80px)",textAlign:"left",backgroundColor:"#FFDDEE",overflow:"scroll"}}>{JSON.stringify(constraintSchema,null,1)}</pre>
              <div>Definition</div>
              <pre style={{maxWidth:"900px",height:"calc(33vh - 80px)",textAlign:"left",backgroundColor:"#EEFFEE",overflow:"scroll"}}>{JSON.stringify(definition,null,1)}</pre>
              <div>Strategy.template.scheduling_constraints_doc for {selectedStrategy?.template?.scheduling_constraints_template?.version}</div>
              
          <pre style={{maxWidth:"900px",height:"calc(33vh - 80px)",textAlign:"left",backgroundColor:"#EEEEEE",overflow:"scroll"}}>{JSON.stringify(selectedStrategy?.template,null,1)}</pre>
          </td>
          <td style={{verticalAlign:"top",maxHeight:"calc(100vh - 80px)",overflow:"scroll",display:"flex"}}>
          <SpecificationDetailsForm disable={disabled}
          definition={definition} 
          isDebugLoggingEnabled={true}
          constraintSchemaCallback={constraintSchemaCallback}
          definitionCallback ={definitionCallback} strategy={selectedStrategy}
          key={selectedStrategy?.url + count}
          taskTemplates={taskTemplates}
          constraintTemplates={constraintTemplates}
          theme="tailwind"
          showInGrid={true}    commonSchemaTemplate={commonSchemaTemplate}  taskFilters={taskFilters}    />
          </td>
          </tr>
          
          </tbody>
          </table>
          
          

    </div>


  )
}

export default App

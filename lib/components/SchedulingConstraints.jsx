import _ from 'lodash';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import moment from 'moment';
import Jeditor from "./JEditor"
import UnitConverter from "../utils/unit.converter"
import { CALENDAR_DATETIME_FORMAT } from '../format';
import ParserUtility from "../utils/parser.utility";
const SchedulingConstraints =  ({
    parentFunction = () => {},
    GetSystemUTC = null,
    constraintTemplate = null,
    initValue = null,
    disable = false,
    formatOutput = false,
    callback = null,
    onUpdate = '',
    Theme,
    showInGrid = false,
    isDebugLoggingEnabled = false,
    allwaysSetInitOnChange = false
}) => {
    
    if (isDebugLoggingEnabled) console.log("SchedulingConstraints props. disable="+disable)
    let editorFunction = null;
    const pshowInGrid = showInGrid || false
    let incommingparentFunction = parentFunction;
    const ParentFunction = incommingparentFunction || ((editorFn) => { editorFunction = editorFn; });
  
    const [constraintSchema, setConstraintSchema] = useState();
    const [initialValue, setInitialValue] = useState();
    const [systemTime, setSystemTime] = useState();
    const [jeditorKey, setJeditorKey] = useState(); 
      
    // Callback function to pass the constraints JSON output to parent component
    const onEditForm = (jsonOutput, errors ) => {
        if (isDebugLoggingEnabled) console.log("- onEditForm: OnEdit Form Triggering callback function",_.cloneDeep(jsonOutput),_.cloneDeep(errors));
        if (jsonOutput.scheduler === "online" || jsonOutput.scheduler === "dynamic") { /* With dynamic time, we can ignore the at, after, before) */
            errors = errors.filter((e) => e.path !== "root.time.at" );
            errors = errors.filter((e) => !e.path.startsWith("specification.time" )); // we don't need the specification time.
            if (isDebugLoggingEnabled) console.log("- onEditForm: Dynamic, Errors after cleaning",_.cloneDeep(errors)); 
          }

        if (jsonOutput.scheduler === "fixed_time"){ // we can ignore one of the at after and before
             const adjustedErrors = errors.filter((e) =>  e.path.startsWith("specification.time" )).length; // get the time errors
             if ( adjustedErrors==1 ||  adjustedErrors ==2  ) {                // it looks like at least one field is valid
              errors = errors.filter((e) => !e.path.startsWith("specification.time" )); // then get rid of the specification errors
             }
             if (isDebugLoggingEnabled) console.log("-onEditForm: Fixed_Time, Errors after cleaning",_.cloneDeep(errors)); 
          }
        
        if (callback) {
            callback(jsonOutput, errors);
        }
    }
  
    useEffect(() => {
    const modifyInitiValue = () => {
        const xinitValue = _.cloneDeep(initValue);
        // For DateTime
        for (let key in xinitValue.time) {
            if (typeof xinitValue.time[key] === 'string') {
                xinitValue.time[key] = moment(new Date((initValue.time[key] || '').replace('Z', ''))).format(CALENDAR_DATETIME_FORMAT);
            } else {
                xinitValue.time[key]?.forEach(time => {
                    for (let subKey in time) {
                        time[subKey] = moment(new Date((time[subKey] || '').replace('Z', ''))).format(CALENDAR_DATETIME_FORMAT);
                    }
                    return true;
                })
            }
        }
        if (xinitValue.time && !xinitValue.time.at) {
            xinitValue.time.at = '';
        }
        if (xinitValue.time && !xinitValue.time.after) {
            xinitValue.time.after = '';
        }
        if (xinitValue.time && !xinitValue.time.before) {
            xinitValue.time.before = '';
        }

        UnitConverter.radiansToDegree(xinitValue.sky);
        
        if (!_.isEqual(xinitValue,initValue) || allwaysSetInitOnChange) {

            if (isDebugLoggingEnabled) console.log("modifyInitiValue: updating now!" + allwaysSetInitOnChange,xinitValue,initValue);
            setInitialValue(xinitValue);
        } else
        {
            if (isDebugLoggingEnabled)  console.log("modifyInitiValue: Updated Ini Value", xinitValue);
        }
    }

    if (initValue) {
        if (isDebugLoggingEnabled)  console.log("xinitValue : There is a init value. let's see if we need changes.", initValue);
        modifyInitiValue();
    }

    },[initValue,allwaysSetInitOnChange,isDebugLoggingEnabled])


        //Configuring Schema Definitions
        const configureDefinitions = (schema) => {
            for (const definitionName in schema.definitions) {
                if (definitionName === 'timestamp') {
                    schema.definitions.timestamp = {
                        validationType: 'datetime',
                        type: schema.definitions.timestamp.type
                    };
                }  else if (definitionName !== 'timewindow' && definitionName !== 'timestamp' &&  definitionName !== 'pointing') {
                    schema.definitions[definitionName] = {
                        type: schema.definitions[definitionName].type
                    };
                } else if (definitionName === 'timewindow') {
                    for (let property in schema.definitions.timewindow.properties) {
                        if (property === 'to' || property === 'from') {
                            if (property === 'from') {
                                schema.definitions.timewindow.properties[property].propertyOrder = 1;
                            } else {
                                schema.definitions.timewindow.properties[property].propertyOrder = 2;
                                schema.definitions.timewindow.properties[property].title = 'until';
                            }
                        }
                    }
                }
            }
        }
    
    useEffect(() => {

        
  //SU Constraint Editor Property Order,format and validation
  const configureProperties = (properties, previouskey="",originalProperties=null) => {

    
    if (originalProperties == null )  originalProperties = properties; // we need this is a reference.

    for (const propertyKey in properties) {
        let propertyValue = properties[propertyKey];
        if (propertyValue instanceof Object) {
 
            if (propertyValue?.$ref=="#/definitions/distance_to_bodies"  ) 
            { // only resolv this one, distance to bodies.
                
             let refnode = ParserUtility.getValueFromJsonPointer(originalProperties,propertyValue.$ref)     // We need to resolve it from the document itself.
             const copyOfRefNode = _.cloneDeep(refnode)   // We are reising the same part, so a deepcopy is needed!
             properties[propertyKey] =   _.assign({}, propertyValue, copyOfRefNode);
             if (isDebugLoggingEnabled) console.log(previouskey + "." + propertyKey,propertyValue,properties[propertyKey])
             propertyValue = properties[propertyKey];
              
            }
            if (propertyKey !== "reference_pointing") {
                configureProperties(propertyValue,previouskey + "." + propertyKey ,originalProperties)
            }
            if (propertyKey === 'scheduler') {
                propertyValue.propertyOrder = 1;
            }
            if (propertyKey === 'time') {
                propertyValue.propertyOrder = 2;
            }
            if (propertyKey === 'at' || propertyKey === 'after' || propertyKey === 'before') {
                propertyValue.propertyOrder = 3;
            }
            if (propertyKey === 'between' || propertyKey === 'not_between') {
                propertyValue.propertyOrder = 4;
            }
            if (propertyKey === 'daily') {
                propertyValue.propertyOrder = 5;
                propertyValue.format = 'checkbox';
                propertyValue.skipFormat = true;
            }
            if ((propertyKey === 'require_night' || propertyKey === 'require_day' || propertyKey === 'avoid_twilight') && typeof (propertyValue) !== "boolean") {
                propertyValue.propertyOrder = 6;
                propertyValue.format = 'checkbox';
                propertyValue.skipFormat = true;
            }
            if (propertyKey === 'sky') {
                propertyValue.propertyOrder = 7;
            }
            if ((propertyKey === 'calibrator' || propertyKey === 'target') && !propertyValue.$ref ) {

                if (onUpdate !== "scheduleConstraints") {
                    propertyValue.default = ((propertyValue.default * 180) / Math.PI).toFixed(2);
                }
                propertyValue.title = propertyKey + ' (Degrees)';
                propertyValue.propertyOrder = 8;
                propertyValue.validationType = 'elevation';
            }
            if (propertyKey === 'transit_offset') {
                propertyValue.propertyOrder = 9;
            }
            if (propertyKey === 'from') {
                propertyValue.propertyOrder = 10;
                propertyValue.validationType = 'transitOffset';
            }
            if (propertyKey === 'to') {
                propertyValue.propertyOrder = 11;
                propertyValue.validationType = 'transitOffset';
            }
            if (propertyKey === 'sun' || propertyKey === 'moon' || propertyKey === 'jupiter') {
                
                if (isDebugLoggingEnabled) console.log("(NOW)" + previouskey + "." + propertyKey + " default -> " + propertyValue.default)
                propertyValue.default = ((propertyValue.default * 180) / Math.PI).toFixed(2);
                if (isDebugLoggingEnabled) console.log("(INTO)" + previouskey + "." + propertyKey + " default -> " + propertyValue.default)
                propertyValue.title = propertyKey + ' (Degrees)';
                propertyValue.propertyOrder = 12;
                propertyValue.validationType = 'distanceOnSky';
            }
        }
    }
};
    

        const constraintStrategy = async () => {
            
            const xconstraintTemplate = _.cloneDeep( constraintTemplate);
           if (isDebugLoggingEnabled) { console.log("Before  constraintstrategy ",xconstraintTemplate)}
            if (xconstraintTemplate.ref_resolved_schema) {
                configureProperties(xconstraintTemplate.ref_resolved_schema.properties,"/", xconstraintTemplate.ref_resolved_schema);
                configureDefinitions(xconstraintTemplate.ref_resolved_schema);
            }
            setConstraintSchema(xconstraintTemplate);
            if (isDebugLoggingEnabled) console.log("After constraintstrategy ",xconstraintTemplate)
            setJeditorKey(`${Date.now()}-${Math.random().toString(36).slice(2, 11)}`);
            return  xconstraintTemplate
        };
       
        if (!constraintTemplate) {
            return;
        }

        if (GetSystemUTC) {
           GetSystemUTC().then(utcTime => {
                setSystemTime(moment.utc(utcTime));
            });
        }

         constraintStrategy();

        

        if (editorFunction) {
            editorFunction();
        }
    }, [constraintTemplate]); // eslint-disable-line react-hooks/exhaustive-deps


    // once a strange typoed parameter was present in the createelement for jeditor. need to find out why and what defintionFormatter: configureDefinitions,
       

    return (
        <>
           {(constraintSchema && jeditorKey)? (
    <Jeditor 
        id="constraint_editor"
        title="Scheduling Constrains specification" 
        schema={constraintSchema.ref_resolved_schema}
        callback={onEditForm} 
        initValue={initialValue}
        disabled={disable}
        formatOutput={formatOutput}
        parentFunction={ParentFunction}
        resolveExtRef={false}
        systemTime={systemTime}
        showInGrid={pshowInGrid}
        isDebugLoggingEnabled={isDebugLoggingEnabled}
        Theme={Theme}
        key={jeditorKey}
    />
) : ""}
        </>
    );
};

SchedulingConstraints.propTypes = {
    initValue: PropTypes.objectOf(PropTypes.any),
    parentFunction: PropTypes.func,
    GetSystemUTC: PropTypes.func,
    constraintTemplate: PropTypes.object,
    disable: PropTypes.bool,
    formatOutput: PropTypes.bool,
    callback: PropTypes.func,
    onUpdate: PropTypes.string,
    Theme: PropTypes.string,
    showInGrid: PropTypes.bool,
    isDebugLoggingEnabled:PropTypes.bool,
    allwaysSetInitOnChange:PropTypes.bool
};


export default SchedulingConstraints;
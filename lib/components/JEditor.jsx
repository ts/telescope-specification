/**
 * This is the custom component to use "@json-editor/json-editor"
 * to create form using JSON Schema and get JSON output
 */
/* eslint-disable react-hooks/exhaustive-deps */
import PropTypes from 'prop-types';
import  { useEffect, useRef } from 'react';
import _ from 'lodash';
import UnitConverter from '../utils/unit.converter';
import Validator from '../utils/validator';

import "@fortawesome/fontawesome-free/css/all.css";
import "flatpickr/dist/flatpickr.css";

import {JSONEditor}  from "@json-editor/json-editor";
const CALENDAR_DEFAULTDATE_FORMAT = 'YYYY-MM-DD'

 function Jeditor(props) {

    const editorRef = useRef(null);
    let pointingProps = useRef(null);
    let subBandProps = useRef(null);
    let stationListPaths =  useRef([]);
    let channelsPerSubbandProps = useRef([]);
    let channelsPerPartProps = useRef([]);
    let channelsFilterbankProps = useRef([]);
    let pipelineAverageProps = useRef([]);
    let skyDistanceProps = useRef([]);
    let skyElevationProps = useRef([]);
    let singlePulseSearch = useRef(false);
    let filterBankEnabled = useRef(false);
    let editor = null;    
    let clockValue = 200000000;
    const theme = props.Theme || "html";
    const isDebugLoggingEnabled = props.isDebugLoggingEnabled;

    if (isDebugLoggingEnabled) console.log("Jeditor props",props);



    //Based on Band pass filter on editor nyquistzone value applied
    const getNyquistzone = (bandpassFilter) => {
        bandpassFilter = bandpassFilter || props.bandPassFilter || '';
        let nyquistzone = 0;
        switch (bandpassFilter) {
            case 'LBA_10_90':
            case 'LBA_30_90':
            case 'LBA_10_70':
            case 'LBA_30_70':
                nyquistzone = 1;
                break;
            case 'HBA_110_190':
                nyquistzone = 2;
                break;
            case 'HBA_170_230':     
            case 'HBA_210_250':              
                nyquistzone = 3;
                break;
            default:
                break;
        }
        return nyquistzone;
    }
    
    //Formula and calculations for subband(minimum,maximum and center) values
    const subband2frequencyCenter = (subband, bandpassFilter) => {
        let subbandbandwidth = clockValue / 1024;
        let frequency = (subband * subbandbandwidth + clockValue / 2 * (getNyquistzone(bandpassFilter) - 1))/1000000;
        return parseFloat(frequency).toFixed(1);
    }

    const subband2frequencyMinimum = (subband, bandpassFilter) => {
        let subbandbandwidth = clockValue / 1024;
        let frequency = ((subband - 0.5) * subbandbandwidth + clockValue / 2 * (getNyquistzone(bandpassFilter) - 1))/1000000;
        return parseFloat(frequency).toFixed(1);
    }

    const subband2frequencyMaximum = (subband, bandpassFilter) => {
        let subbandbandwidth = clockValue / 1024;
        let frequency = ((subband + 0.5) * subbandbandwidth + clockValue / 2 * (getNyquistzone(bandpassFilter) - 1))/1000000;
        return parseFloat(frequency).toFixed(1);
    }


    const subband2frequencyRange = (subband1, subband2, bandpassFilter) => {
        let min_frequency = subband2frequencyMinimum(subband1, bandpassFilter);
        let max_frequency = subband2frequencyMaximum(subband2, bandpassFilter);
        return `${min_frequency}-${max_frequency}`;
    }
    
    const init = async () => {
        const element = document.getElementById(props.id?props.id:'editor_holder');
        if (element?.firstChild) {
            element.removeChild(element.firstChild);
        }
     
            
       let schema = props.schema;
      
        /** If any formatting is done at the parent/implementation component pass the resolved schema 
            and get the formatted schema like adding validation type, field ordering, etc.,*/
        pointingProps = [];
        subBandProps = [];
        channelsPerSubbandProps = [];
        channelsPerPartProps = [];
        channelsFilterbankProps = [];
        stationListPaths = [];
        pipelineAverageProps = [];
        skyDistanceProps = [];
        skyElevationProps = [];
        singlePulseSearch = false;
        filterBankEnabled = false;
        // durationProps = [];
        // Customize the pointing property to capture angle1 and angle2 to specified format

        for (const definitionKey in schema?.definitions) {
            if (definitionKey === 'pointing') {
                const defintion = schema?.definitions[definitionKey];
                let defProperties = defintion.properties;
                if (defProperties) {
                    for (const propName in defProperties) {
                        if (propName === 'angle1' || propName === 'angle2') {
                            let defProperty = getAngleProperty(defProperties[propName], propName === 'angle2');
                            defProperties[propName] = defProperty;
                        }
                        if (props.showInGrid) {
                            if (defProperties[propName].options) {
                                defProperties[propName].options.grid_columns = 4;
                            } else {
                                defProperties[propName].options = { grid_columns: 4 };
                            }
                        }
                    }
                }
            }
        }        

        // Customize datatype of certain properties like subbands, duration, etc.,
        getCustomProperties(schema?.properties);
        getCustomProperties(schema?.definitions);
        // Update the station list $ref with actual station definition to display the station as 'Select' list
        if (schema?.definitions?.station_list?.items?.["$ref"] && 
            schema?.definitions?.station_list.items?.["$ref"]==="#/definitions/station" &&
            schema?.definitions?.station) {
            // Before updating the station list 4ref, for all known station group, $ref is made to refer the old station list $ref 
            // so that predifined stations are pre-selected and not editable
            schema.definitions["station_group_list"] =  _.cloneDeep(schema.definitions.station_list);
            if (schema.definitions.station_group) {
                for (const anyOf of schema.definitions.station_group.anyOf) {
                    if (anyOf.title !== "Custom") {
                        anyOf.properties.stations.$ref = "#/definitions/station_group_list";
                    }
                }
            }
            schema.definitions.station_list.items = schema.definitions.station;
        }
        if (schema) {
            schema.title = props.title;
        }
        const subbandValidator = validateSubbandOutput;
        const timeAngleValidator = Validator.validateTimeAndAngle;
        const angleValidator = Validator.validateAngle;
        JSONEditor?.defaults?.custom_validators.push((schema, value, path) => {
            const errors = [];
            if (schema.validationType === "subband_list") {
                if (!subbandValidator(value, path)) {
                    if(_.indexOf(subBandProps, path) === -1) {
                        subBandProps.push(path)
                    }
                    errors.push({
                        path: path,
                        property: 'validationType',
                        message: 'Not a valid input for Subband List'
                    });
                }
            } else if (schema?.validationType === "subband_list_optional") {
                if (value && !subbandValidator(value, path)) {                
                    if(_.indexOf(subBandProps, path) === -1) {
                        subBandProps.push(path)
                    }
                    errors.push({
                        path: path,
                        property: 'validationType',
                        message: 'Not a valid input for Subband List'
                    });
                }
            } else if (schema?.validationType === "channelsPerPart") {
                // To add eventlistener to the channels_per_part field based on the validation type set
                if(_.indexOf(channelsPerPartProps, path) === -1) {
                    channelsPerPartProps.push(path)
                }
            } else if (schema?.validationType === "channelsFilterbank") {
                // To add eventlistener to the channels_per_part field based on the validation type set
                if(_.indexOf(channelsFilterbankProps, path) === -1) {
                    channelsFilterbankProps.push(path)
                }
            } else if (schema?.validationType === "channelsPerSubband") { 
                // To add eventlistener to the channels_per_subband field based on the validation type set
                if(_.indexOf(channelsPerSubbandProps, path) === -1) {
                    channelsPerSubbandProps.push(path)
                }
            } else if(schema?.validationType === "pipelineAverage") {
                // To add eventlistener to the fields of pipeline average based on the validation type set
                if(_.indexOf(pipelineAverageProps, path) === -1) {
                    pipelineAverageProps.push(path)
                }
            } else if (schema?.validationType === "time") {
                if (!timeAngleValidator(value)) {
                    errors.push({
                        path: path,
                        property: 'validationType',
                        message: 'Not a valid input. Mimimum: 00:00:00.0000hours or 0 or 00:00:00.0000degrees, Maximum:23:59:59.9999hours or 6.2831 or 359:59:59.9999degrees'
                    });
                }
            } else if (schema?.validationType === "angle") {
                if (!angleValidator(value)) {
                    errors.push({
                        path: path,
                        property: 'validationType',
                        message: 'Not a valid input. Mimimum: -90:00:00.0000degrees 0r -1.57079632679489661923, Maximum:90:00:00.0000degrees or 1.57079632679489661923'
                    });
                }
            } else if (schema?.validationType === "transitOffset") {
                Validator.validateTransitOffset(schema, value, errors, path);
            } else if (schema?.validationType === "duration_HHmmss") {
                Validator.validateDurationHHmmss(schema, value, errors, path);
            } else if (schema?.validationType === "distanceOnSky") {
                // To add eventlistener to the sky distance field based on the validation type set
                if(_.indexOf(skyDistanceProps, path) === -1) {
                    if(editor?.editors?.[path]?.formname) {
                        skyDistanceProps.push(path)
                        let skyDegreeElement = document.getElementsByName(editor.editors[path].formname)[0];
                        if(skyDegreeElement) {
                            skyDegreeElement.addEventListener('keyup', (event) => { 
                                if(event.currentTarget.value) {                      
                                    let value = event.currentTarget.value; 
                                    const skyPattern = /^\d{1,3}(\.\d{1,2})?$/;
                                    if (!skyPattern.test(value)){
                                        skyDegreeElement.value = value.substring(0, value.indexOf(".")+3);
                                    }
                                }
                            });
                        }
                    }
                }
                if (value === '' || value === undefined || value === null || isNaN(value) || value < 0 || value > 180) {
                    errors.push({
                        path: path,
                        property: 'validationType',
                        message: 'Not a valid input. Must be number between 0 - 180'
                    });
                }
            } else if (schema?.validationType === "elevation") {
                // To add eventlistener to the sky elevation fields based on the validation type set
                if(_.indexOf(skyElevationProps, path) === -1) {
                    if(editor?.editors?.[path]?.formname) {
                        skyElevationProps.push(path)
                        let skyElevationElement = document.getElementsByName(editor.editors[path].formname)[0];
                        if(skyElevationElement) {
                            skyElevationElement.addEventListener('keyup', (event) => { 
                                if(event.currentTarget.value) {                      
                                    let value = event.currentTarget.value; 
                                    const skyPattern = /^\d{1,2}(\.\d{1,2})?$/;            
                                    if (!skyPattern.test(value)){
                                        skyElevationElement.value = value.substring(0, value.indexOf(".")+3);
                                    }
                                }
                            });
                        }
                    }
                }
                if (isNaN(value) || value < 0 || value > 90) {
                    if (isDebugLoggingEnabled)  console.log('Not a valid input. Must be number between 0 - 90. Given Value: ' , value);
                    errors.push({
                        path: path,
                        property: 'validationType',
                        message: 'Not a valid input. Must be number between 0 - 90.  '
                    });
                }
            } else if (schema.max_length) {
                if (value && value.length>schema.max_length) {
                    errors.push({
                        path: path,
                        property: 'validationType',
                        message: 'Exceed maximum allowed characters, enter the max length '+schema.max_length+' in '+schema.label
                    });
                }
            }
            return errors;
        });
        if (props.showInGrid) {
            schema.format = "grid";
        }
        const editorOptions = {
            form_name_root: "specification",
            schema: schema,
            theme: theme,
            iconlib: 'fontawesome5',
            display_required_only: false,
            remove_button_labels: true,
            disable_edit_json: true,
            disable_properties: true,
            disable_collapse: false,
            prompt_before_delete: false,
            show_errors: props.errorsOn ? props.errorsOn : 'change',        // Can be 'interaction', 'change', 'always', 'never'
            compact: true,
            ajax: true,

            object_layout: props.showInGrid?"grid":"normal"
        };
        // Set Initial value to the editor
        if (props.initValue) {
            let startVal = _.cloneDeep(props.initValue);
            if (startVal.$schema) {
                delete startVal.$schema;
            }
            singlePulseSearch = (startVal['single_pulse_search'])?true:false;
            if (isDebugLoggingEnabled)   console.log("editorOptions",editorOptions);
            editorOptions.startval = updateInput(startVal);
            //editorOptions.schema.default = updateInput(startVal);
        }
        let editor
        try {
            if (element!==null && typeof element !== 'undefined') {
                editor = new JSONEditor(element, editorOptions);
            }
            else
            if (isDebugLoggingEnabled)   console.log("Cannot Create JsonEditor, null element");
        }
        catch (ex) {
            console.info("Failure in Creating editor",ex);
        }
        

        if (document!==null){
        // Remove loader div after editor intialization
        const loadingElement = document?.getElementById(props.id?`${props.id}_loader`:'editor_holder_loader');
        if (loadingElement?.firstChild) {
            loadingElement.removeChild(loadingElement.firstChild);
        }
    }
        
        editorRef.current = editor;
        editor?.on('ready',() => {
            if (props.disabled) {
                editor?.disable();
            }
            if (props.parentFunction) {
                props.parentFunction(editorFunction);
            }
        });
        editor?.on('change',() => {   
            setEditorOutput();
        }); 
        // Called when a row is deleted
        // editor.on('deleteRow', (e) => {
        //     console.log(e);
        // });
        //adding onchange event for subband
        if(subBandProps) {
            subBandProps.forEach(function(pathName){
                if(editor?.editors[pathName]?.formname) {
                    let elemetName = document.getElementsByName(editor.editors[pathName].formname)[0];
                    if(elemetName) {
                        elemetName.addEventListener('keydown', (event) => { 
                            if(event.currentTarget.value) {                       
                                validateSubbandOutput(event.currentTarget.value, pathName);
                            }
                        })
                    }
                 }
            });
        }
        // Filter all stations select tag and get the names to add chips below station select element
        for (const selectElement of document.getElementsByTagName("select")) {
            if (selectElement.name.endsWith("[stations]")) {
                stationListPaths.push(selectElement.name);
            }
        }
        // Add Onchange event for Channels Per Subband field and Time Integration field to update Frequency Resolution and Time Resolution
        for (const channelPath of channelsPerSubbandProps) {
            if(editor?.editors[channelPath]?.formname) {
                let channelsPerSubElement = document.getElementsByName(editor.editors[channelPath].formname)[0];
                if(channelsPerSubElement) {
                    channelsPerSubElement.addEventListener('change', (event) => { 
                        if(event.currentTarget.value) {                       
                            updateFrequencyResolution(channelPath, event.currentTarget.value);
                            updateTimeResolution(channelPath, event.currentTarget.value, null);
                        }
                    });
                    // Add event listener for Time Integration field
                    let intTimePath = channelPath.split(".");
                    intTimePath[intTimePath.length-1] = props.observationType==='beamforming observation'?'time_integration_factor':'integration_time';
                    intTimePath = intTimePath.join(".");
                    if (editor.editors[intTimePath]?.formname) {
                        let intTimeElement = document.getElementsByName(editor.editors[intTimePath].formname)[0];
                        if(intTimeElement) {
                            intTimeElement.addEventListener('keyup', (event) => { 
                                updateTimeResolution(intTimePath, null, event.currentTarget.value || 0);
                            });
                        }
                    }
                    // Disable Frequncey Resolution field
                    let freqResPath = channelPath.split(".");
                    freqResPath[freqResPath.length-1] = "frequency_resolution";
                    freqResPath = freqResPath.join(".")
                    if(editor.editors[freqResPath]?.formname) { 
                        document.getElementsByName(editor.editors[freqResPath]?.formname)[0].disabled = true;
                    }
                    // Disable Time resolution field
                    let timeResPath = channelPath.split(".");
                    timeResPath[timeResPath.length-1] = "time_resolution";
                    timeResPath = timeResPath.join(".");
                    if(editor.editors[timeResPath]?.formname) { 
                        document.getElementsByName(editor.editors[timeResPath]?.formname)[0].disabled = true;
                    }
                }
             }
        }

        // Add Onchange event for Channels Per Part field and Time Integration field to update Frequency Resolution and Time Resolution
        for (const channelPath of channelsFilterbankProps) {
            if(editor?.editors[channelPath]?.formname) {
                let channelsFilterbankElement = document.getElementsByName(editor.editors[channelPath].formname)[0];
                if(channelsFilterbankElement) {
                    // Disable Frequncey Resolution field
                    let freqResPath = channelPath.split(".");
                    freqResPath[freqResPath.length-1] = "frequency_resolution";
                    freqResPath = freqResPath.join(".")
                    if(editor.editors[freqResPath]?.formname) { 
                        document.getElementsByName(editor.editors[freqResPath]?.formname)[0].disabled = true;
                    }

                    let filterbankPath = channelPath.split(".");
                    filterbankPath[filterbankPath.length-1] = "enabled";
                    filterbankPath = filterbankPath.join(".")
                    if(editor.editors[filterbankPath]?.formname) {                        
                        document.getElementsByName(editor.editors[filterbankPath]?.formname)[0].addEventListener('change', (event) => { 
                            filterBankEnabled =event.currentTarget.value === '1'
                            updatePipelineFrequencyResolution(channelPath, null, false);
                        });
                        filterBankEnabled = document.getElementsByName(editor.editors[filterbankPath]?.formname)[0].value === '1'
                    }

                    channelsFilterbankElement.addEventListener('keyup', (event) => { 
                        if(event.currentTarget.value) {                       
                            updatePipelineFrequencyResolution(channelPath, event.currentTarget.value, false);
                        }
                    });
                }
             }
        } 

        // Add Onchange event for Channels Per Part field and Time Integration field to update Frequency Resolution and Time Resolution
        for (const channelPath of channelsPerPartProps) {
            if(editor?.editors[channelPath]?.formname) {
                let channelsPerPartElement = document.getElementsByName(editor.editors[channelPath].formname)[0];
                if(channelsPerPartElement) {
                    let freqResolution = null;
                    channelsPerPartElement.addEventListener('keyup', (event) => { 
                        if(event.currentTarget.value) {
                            freqResolution = updatePipelineFrequencyResolution(channelPath, event.currentTarget.value, true);
                            updatePipelineTimeResolution(channelPath, freqResolution, null);
                        }
                    });
                    // Add event listener for Integration Time field
                    let intTimePath = channelPath.split(".");
                    intTimePath[intTimePath.length-1] = props.observationType==='pulsar pipeline'?'integration_time_factor':'integration_time';
                    intTimePath = intTimePath.join(".");
                    if (editor.editors[intTimePath]?.formname) {
                        let intTimeElement = document.getElementsByName(editor.editors[intTimePath].formname)[0];
                        if(intTimeElement) {
                            intTimeElement.addEventListener('keyup', (event) => { 
                                updatePipelineTimeResolution(intTimePath, freqResolution, event.currentTarget.value || 0);
                            });
                        }
                    }

                    if (editor.editors[intTimePath]?.formname) {
                        let intTimeElement = document.getElementsByName('specification[single_pulse_search]')[0];
                        if(intTimeElement) {
                            intTimeElement.addEventListener('change', (event) => { 
                                singlePulseSearch =event.currentTarget.value === '1';
                                freqResolution = updatePipelineFrequencyResolution(channelPath, null, true);
                                updatePipelineTimeResolution(channelPath, freqResolution, null);
                            });
                        }
                    }
                    
                   // 'specification[single_pulse_search]'
                    // Disable Frequncey Resolution field
                    let freqResPath = channelPath.split(".");
                    freqResPath[freqResPath.length-1] = "frequency_resolution";
                    freqResPath = freqResPath.join(".")
                    if(editor.editors[freqResPath]?.formname) { 
                        document.getElementsByName(editor.editors[freqResPath]?.formname)[0].disabled = true;
                    }
                    // Disable Time resolution field
                    let timeResPath = channelPath.split(".");
                    timeResPath[timeResPath.length-1] = "time_resolution";
                    timeResPath = timeResPath.join(".");
                    if(editor.editors[timeResPath]?.formname) { 
                        document.getElementsByName(editor.editors[timeResPath]?.formname)[0].disabled = true;
                    }
                }
             }
        }       

        // Add Onchange event for pipeline average time_step field and frequency_step field to update Frequency Resolution and Time Resolution
        for (const pipelineAvgPath of pipelineAverageProps) {
            if(editor?.editors[pipelineAvgPath]?.formname) {
                let pipelineAvgElement = document.getElementsByName(editor.editors[pipelineAvgPath].formname)[0];
                if(pipelineAvgElement) {
                    const pipelineAvgPathSplit = pipelineAvgPath.split(".");
                    if (pipelineAvgPathSplit[pipelineAvgPathSplit.length-1] === 'time_steps') {
                        pipelineAvgElement.addEventListener('keyup', (event) => { 
                            if(event.currentTarget.value) {   
                                updateTimeResolution(pipelineAvgPath, 'dummy', event.currentTarget.value || 0);
                            }
                        });
                        // Disable Time resolution field
                        let timeResPath = pipelineAvgPath.split(".");
                        timeResPath[timeResPath.length-1] = "time_resolution";
                        timeResPath = timeResPath.join(".");
                        if(editor.editors[timeResPath]?.formname) { 
                            document.getElementsByName(editor.editors[timeResPath]?.formname)[0].disabled = true;
                        }
                    }   else {
                        pipelineAvgElement.addEventListener('keyup', (event) => { 
                            if(event.currentTarget.value) {             
                                updateFrequencyResolution(pipelineAvgPath, null, event.currentTarget.value || 0);
                            }
                        });
                        // Disable Frequncey Resolution field
                        let freqResPath = pipelineAvgPath.split(".");
                        freqResPath[freqResPath.length-1] = "frequency_resolution";
                        freqResPath = freqResPath.join(".")
                        if(editor.editors[freqResPath]?.formname) { 
                            document.getElementsByName(editor.editors[freqResPath]?.formname)[0].disabled = true;
                        }
                    }
                }
             }
        }

        while (element?.childNodes.length > 1) {
            element.removeChild(element.firstChild);
        }
        // To disable any specific fields in the editor, pass them from the parent component. 
        // For example in the schema if it is {SAPs:[{name:'abc',...}, {name:'xyz',...}]} values to be passed to disable name fields
        // ['SAPs.0.name', 'SAPs.1.name']
        if(props.fieldsToDisable && editor?.editors) {
            for(const fieldToDisable of props.fieldsToDisable) {
                let rootElement = 'root';
                if (!editor.editors[rootElement]) {
                    rootElement = 'specification';
                }
                if (editor && editor.editors[rootElement + '.'+fieldToDisable] &&  editor.editors[rootElement + '.'+fieldToDisable].formname) {
                    document.getElementsByName(editor.editors[rootElement + '.'+fieldToDisable].formname)[0].disabled = true;
                }
            }
        }
    };

    useEffect(() => {
        if (!editor) {
            init();
        }
    },[props.schema]);

    /**
     * Function to call on button click and send the output back to parent through callback
     * 
     */
    function setEditorOutput() {   
        if (editorRef.current.ready===false) return;
        const editorOutput = editorRef.current.getValue();     
        /* Sends editor output without formatting if requested */
        const formatOutput = props.formatOutput === undefined ? true : props.formatOutput;
        if (isDebugLoggingEnabled) console.log("setEditorOutput formatOutput ="+formatOutput);
        const formattedOutput = formatOutput ? updateOutput(_.cloneDeep(editorOutput)) : _.cloneDeep(editorOutput);
        const editorValidationErrors = editorRef.current.validate();
        if (props.callback) {
            // editorRef.current for accessing fields in parent to add classname for enabling and disabling
            props.callback(formattedOutput, editorValidationErrors, editorRef.current);
        }
        // When new station group is added, the select element needs to be considered to show selected station. So re-iterate station select tags
        stationListPaths = [];
        for (const selectElement of document.getElementsByTagName("select")) {
            if (selectElement.name.endsWith("[stations]")) {
                stationListPaths.push(selectElement.name);
            }
        }
        // Call function to create Selected Station List Tag
        addSelectedStationTags();
    }

    /**
     * Function called by the parent component to perform certain action ib JEditor
     */
    function editorFunction(name, params) {
        if (name === "setValue") {
            const newValue = updateInput(_.cloneDeep(params[0]));
            editorRef.current.setValue(newValue);
        } else {
            editorRef.current.destroy();
        }
    }

    /**
     * Function to convert the angle fields in HH:mm:ss or DD:mm:ss format based on isDegree value.
     * @param {Object} defProperty 
     * @param {Boolean} isDegree 
     */
    function getAngleProperty(defProperty, isDegree) {
        let newProperty = {
            type: "string",
            title: defProperty.title,
            description: defProperty.description.indexOf("(Supported Formats:")>=0?defProperty.description:
                (defProperty.description + (isDegree ?
                "(Supported Formats: '10d15m10.1234s', '10:15:10.1234degrees', '10.2528degrees', '0.1789')" :
                "(Supported Formats: '10h15m10.1234s', '10:15:10.1234hours', '10.4187hours', '10d15m10.1234s', '10:15:10.1234degrees', '10.2528degrees', '2.7276')")),
            default: "0",
            validationType: isDegree ? 'angle' : 'time',
            options: {
                "inputAttributes": {
                    "placeholder": isDegree ? "Degrees or Radian" : "Hours or Radian"
                }
            }
        }
        if (props.showInGrid) {
            newProperty.options.grid_columns = 4;
        }
        return newProperty;
    }

    /**
     * Function to get the schema change for specified properties like subbands, duration, column width, etc
     * @param {Object} properties 
     */
    function getCustomProperties(properties, parentProps) {     
        for (const propertyKey in properties) {
            parentProps = parentProps || [];
            parentProps[propertyKey] = properties[propertyKey];
            const propertyValue = properties[propertyKey];
            if ((propertyKey.toLowerCase() === 'subbands' ||
                propertyKey.toLowerCase() === 'list') && propertyValue.type === 'array') {                 
                    let newProperty = {};
                    newProperty.additionalItems = false;
                    newProperty.title = propertyValue.title;
                    newProperty.type = 'string';
                    newProperty.default = '';
                    newProperty.description = "For Range enter Start and End seperated by 2 dots. Mulitple ranges can be separated by comma. Minimum should be 0 and maximum should be 511. For exmaple 11..20, 30..50";
                    newProperty.validationType = propertyKey.toLowerCase() === 'subbands' ? 'subband_list' : 'subband_list_optional';
                    properties[propertyKey] = newProperty;
                    //Displayed Frequency   
                    if( propertyKey.toLowerCase() === 'subbands' && propertyValue.type === 'array') {
                        let frequency = {};
                        frequency.title = 'Frequency(MHz)';
                        frequency.type = 'string';
                        frequency.default = '';
                        if (props.showInGrid) {
                            frequency.format = "grid"
                        }
                        frequency.validationType = 'frequency';                
                        frequency.propertyOrder = 1002;                   
                        properties['frequency'] = frequency;
                    }
            }   else if (propertyKey.toLowerCase() === 'channels_per_subband' && propertyValue instanceof Object) {
                // Add custom fields to the properties where property 'channels_per_subband' is one of the property.
                let freqResolution = { title: 'Frequency Resolution (kHz)',
                                        type: 'string',
                                        default: ''
                                    };
                if (props.showInGrid) {
                    freqResolution.format = "grid";
                }
                properties['frequency_resolution'] = freqResolution;
                let timeResolution = { title: `Time Resolution (${props.observationType==='beamforming observation'?'ms':'sec'})`,
                                        type: 'string',
                                        default: ''
                                    };
                if (props.showInGrid) {
                    timeResolution.format = "grid";
                }
                properties['time_resolution'] = timeResolution;
                // Set validation type to channels_per_subband to add event listener.
                properties['channels_per_subband']['validationType'] = "channelsPerSubband";
                // properties['channels_per_subband']['propertyOrder'] = 1001
                //>>>>>> If properyOrder is nor working, remove these lines
                for (let property of _.keys(properties)) {
                    if (['channels_per_subband', 'frequency_resolution', 'time_resolution'].indexOf(property)<0) {
                        properties[property]['propertyOrder'] = 1;
                    }
                }//<<<<<<
            }   else if (propertyKey.toLowerCase() === 'single_pulse_search' && props.observationType === 'pulsar pipeline' && properties['dspsr']) {
                // Add custom fields to the properties where property 'single_pulse_search' is one of the property.
                let freqResolution = { title: 'Frequency Resolution (kHz)',type: 'string',default: '' };
                properties['dspsr']['properties']['digifil']['properties']['frequency_resolution'] = freqResolution;
                properties['dspsr']['properties']['filterbank']['properties']['frequency_resolution'] = freqResolution;
                
                // Set validation type to channels_per_part to add event listener.
                properties['dspsr']['properties']['digifil']['properties']['channels_per_part']['validationType'] = "channelsPerPart";
                properties['dspsr']['properties']['filterbank']['properties']['channels_per_part']['validationType'] = "channelsFilterbank";
                
                let timeResolution = { title: `Time Resolution (${props.observationType==='pulsar pipeline'?'ms':'sec'})`, type: 'string', default: '' };
                if (props.showInGrid) {
                    timeResolution.format = "grid";
                }
                properties['dspsr']['properties']['digifil']['properties']['time_resolution'] = timeResolution;
            }   else if (props.targetObservation && propertyKey.toLowerCase() === 'average' && propertyValue instanceof Object) {
                // Add custom fields to the properties where property 'average' is one of the property.
                let timeResolution = { title: `Time Resolution (sec)`,
                                        type: 'string',
                                        default: ''
                                    };
                if (props.showInGrid) {
                    timeResolution.format = "grid";
                    timeResolution.options = {grid_columns: 3};
                }
                propertyValue.properties['time_resolution'] = timeResolution;
                let freqResolution = { title: 'Frequency Resolution (kHz)',
                                        type: 'string',
                                        default: ''
                                    };
                if (props.showInGrid) {
                    freqResolution.format = "grid";
                    freqResolution.options = {grid_columns: 3};
                }
                propertyValue.properties['frequency_resolution'] = freqResolution;
                // Set validation type to channels_per_subband to add event listener.
                propertyValue.propertyOrder = 1;
                if (props.showInGrid) {
                    propertyValue.format = 'grid';
                }
                propertyValue.required.push('frequency_resolution');
                propertyValue.required.push('time_resolution');
                propertyValue.properties['time_steps']['validationType'] = "pipelineAverage";
                propertyValue.properties['frequency_steps']['validationType'] = "pipelineAverage";
                if (props.showInGrid) {
                    propertyValue.properties['time_steps']['format'] = "grid";
                    propertyValue.properties['time_steps']['options'] = { "grid_columns": 3 };
                    propertyValue.properties['frequency_steps']['format'] = "grid";
                    propertyValue.properties['frequency_steps']['options'] = { "grid_columns": 3 };
                }
            }   else if (propertyKey.toLowerCase() === 'duration') {                        
                let newProperty = {
                    "type": "string",
                    "title": "Duration",
                    "description": `${propertyValue.description?.replace("(seconds)","")} (Hours:Minutes:Seconds)`,
                    'validationType': 'duration_HHmmss',
                    "options": {
                        "inputAttributes": {
                            "placeholder": "HH:mm:ss"
                        },
                    },
                    "minimum": propertyValue.minimum || 0,
                    "maximum": propertyValue.maximum || 86400, 
                };
                if (props.showInGrid) {
                    newProperty.options["grid_columns"] = 3
                }
                properties[propertyKey] = newProperty;
            }     
           
            else if (propertyKey.toLowerCase() === 'timedelta') {
                /* empty */
            }   else if ( propertyValue?.['$ref'] && propertyValue?.['$ref'].toLowerCase().indexOf('#/definitions/timestamp')>=0) {
                let newProperty = {};
                newProperty.format = 'datetime-local';
                newProperty.validationType = 'dateTime';
                newProperty.skipFormat = true;
                newProperty.options = {
                    "inputAttributes": {
                        "placeholder": "YYYY-MM-DD HH:mm:ss"
                    },
                    "flatpickr": {
                        "inlineHideInput": true,
                        "wrap": true,
                        "enableSeconds": true,
                        "time_24hr": true,
                        "allowInput": true,
                        "minuteIncrement": 1
                    }          
                };
                if (props.systemTime) {
                    const systemTime = props.systemTime
                    newProperty.options.flatpickr["defaultDate"] = systemTime.format(CALENDAR_DEFAULTDATE_FORMAT);
                    newProperty.options.flatpickr["defaultHour"] = systemTime.hour();
                    newProperty.options.flatpickr["defaultMinute"] = systemTime.minutes();
                }
                properties[propertyKey] = _.merge({}, properties[propertyKey], newProperty);
            }   else if (propertyValue?.['$ref'] && propertyValue?.['$ref'].toLowerCase().indexOf('#/definitions/timedelta')>=0) {
                //TODO: Customization is applied only for constraints transit_offset fields as it is required to keep just time value (seconds) in some places like integration_time
                // set 'required' field value, it required in fields validation
                if (parentProps?.transit_offset) {
                    propertyValue["required"] =parentProps.transit_offset.required? _.includes(parentProps.transit_offset.required, propertyKey):false;
                    propertyValue["type"] = "string";
                    propertyValue["title"] = propertyValue["title"] || propertyKey.toLowerCase();
                    propertyValue["description"] = `${propertyValue.description?propertyValue.description:''} (+/- Hours:Minutes:Seconds)`;
                    propertyValue["options"] = {
                                                "inputAttributes": {
                                                    "placeholder": "+/- Hours:Minutes:Seconds"
                                                }};
                    if (props.showInGrid) {
                        propertyValue["options"]["grid_columns"] = 3
                    }
                    delete propertyValue['$ref'];
                    properties[propertyKey] = _.cloneDeep(propertyValue);
                }
            }   else if (propertyValue instanceof Object) {
                // by default previously, all field will have format as Grid, but this will fail for calendar, so added property called skipFormat
                if (propertyKey !== 'properties' && propertyKey !== 'default' && !propertyValue.skipFormat && props.showInGrid) {
                    propertyValue.format = "grid";
                }
                if (propertyKey === 'average' || propertyKey === 'calibrator' || propertyKey === 'stations') {
                    propertyValue.propertyOrder = 1;
                } else if (propertyKey === 'demix') {
                    propertyValue.propertyOrder = 2;
                } else if (propertyKey === 'QA' || propertyKey === 'beams') {
                    propertyValue.propertyOrder = 10000;
                }
                if (propertyKey === 'storage_cluster' || propertyKey === 'integration_time' || propertyKey === 'storage_manager') {
                    let options = propertyValue.options ? propertyValue.options : {};
                    if (props.showInGrid) {
                        options.grid_columns = 3;
                    }
                    propertyValue.options = options;
                } else if (propertyKey === 'flag') {
                    let options = propertyValue.options ? propertyValue.options : {};
                    if (props.showInGrid) {
                        options.grid_columns = 9;
                    }
                    propertyValue.options = options;
                }
                if (propertyValue['$ref'] && propertyValue['$ref'].endsWith("/pointing")) {
                    pointingProps.push(propertyKey);
                }
                
                getCustomProperties(propertyValue, parentProps);
            }
        }
    }

    /**
     * Function to format the input for custom fields when the editor receive the inital values from the parent component
     * @param {*} editorInput 
     */
    function updateInput(editorInput , originalInput =null) {
        for (const inputKey in editorInput) {
            const inputValue = editorInput[inputKey];       
            if (inputValue instanceof Object) { 
                if (_.indexOf(pointingProps, inputKey) >= 0) {
                    if (isDebugLoggingEnabled) console.log("inputValue.angle1 now" +  inputValue.angle1);

                    inputValue.angle1 = UnitConverter.getAngleInput(inputValue.angle1);
                    if (isDebugLoggingEnabled) console.log("inputValue.angle1 updated" +  inputValue.angle1);

                    inputValue.angle2 = UnitConverter.getAngleInput(inputValue.angle2, true);
                } 
                else if ((inputKey.toLowerCase() === 'subbands' && inputValue instanceof Array) ||
                    (inputKey.toLowerCase() === 'list' && inputValue instanceof Array)) {                 
                        editorInput[inputKey] = getSubbandInput(inputValue);
                        editorInput['frequency'] =  getFrequencyFromSubbandOutput([editorInput[inputKey]],originalInput?.station_configuration?.filter); //   --> Here was written : subband2frequencyRange; , But that is really strange, so i reverted the original null.
                }   else if (inputKey.toLowerCase() === 'average' && props.targetObservation) {
                    inputValue['time_resolution'] = getTimeResolution('dummy', inputValue['time_steps']);
                    inputValue['frequency_resolution'] = getFrequencyResolution('dummy', inputValue['frequency_steps']);
                    updateInput(inputValue,originalInput || editorInput);
                }
                else {                   
                   updateInput(inputValue,originalInput ||editorInput);
                }
            }  
            else if (inputKey.toLowerCase() === 'channels_per_subband') {
                // Add calculated values for custom fields frequency_resolution and time_resolution
                editorInput['frequency_resolution'] = getFrequencyResolution(inputValue);
                const integrationTime = editorInput['integration_time'] || editorInput['time_integration_factor'];
                editorInput['time_resolution'] = getTimeResolution(inputValue, integrationTime);
            }       
            else if (inputKey.toLowerCase() === 'from' || inputKey.toLowerCase() === 'to') {
                // Add calculated values for custom fields frequency_resolution and time_resolution
                editorInput[inputKey] = UnitConverter.getSecsToHHmmssWithSign(inputValue);
            }      
            else if (inputKey.toLowerCase() === 'duration') {
                // }  else if (durationProps.indexOf(inputKey)) {
                editorInput[inputKey] = getTimeInput(inputValue);
            }
            else if (inputKey.toLowerCase() === 'channels_per_part') {
                let isDigifil = false;
                if (editorInput['integration_time_factor']) {
                    isDigifil = true;
                     // Add calculated values for custom fields frequency_resolution and time_resolution
                    const freqResolution = getPipelineFrequencyResolution(inputValue, isDigifil);
                    editorInput['frequency_resolution'] = freqResolution!=='N/A'?(freqResolution/1000).toFixed(2):freqResolution;
                    const integrationTime = editorInput['integration_time_factor']; 
                    const timeResolution = getPipelineTimeResolution(freqResolution, integrationTime)
                    editorInput['time_resolution'] = timeResolution!=='N/A'?(timeResolution*1000).toFixed(5):timeResolution;
                }   else {
                    filterBankEnabled = editorInput['enabled'];
                    const freqResolution = getPipelineFrequencyResolution(inputValue, isDigifil);
                    editorInput['frequency_resolution'] = freqResolution!=='N/A'?(freqResolution/1000).toFixed(2):freqResolution;
                }
            }
        }
        if (isDebugLoggingEnabled) console.log("editorInput", editorInput);
        return editorInput;
    }

    /**
     * Function to format the output of the customized fields
     * @param {*} editorOutput 
     */
    function updateOutput(editorOutput) {
        
        if (isDebugLoggingEnabled)  console.log("going to updateOutput for ",editorOutput);
        for (const outputKey in editorOutput) {
            let outputValue = editorOutput[outputKey];
            if (isDebugLoggingEnabled) console.log("outputKey",outputValue +" -" +  typeof(outputValue))
            if (outputValue instanceof Object) {
                if (isDebugLoggingEnabled) console.log("outputKey in instance off loop ",outputKey,outputValue)
                if (_.indexOf(pointingProps, outputKey) >= 0) {
                    if (isDebugLoggingEnabled)  console.log("outputKey indexOf>=0",outputKey,outputValue)
                    outputValue.angle1 = UnitConverter.parseAngle(outputValue.angle1);
                    outputValue.angle2 = UnitConverter.parseAngle(outputValue.angle2);
                    if (isDebugLoggingEnabled) console.log("outputKey indexOf>=0 Updated",outputKey,outputValue)
                } else {
                    if (isDebugLoggingEnabled) console.log("Left Alone outputKey indexOf>=0 Updated",outputKey,outputValue)
                    updateOutput(outputValue);
                }
            } else if ((outputKey.toLowerCase() === 'subbands' && typeof (outputValue) === 'string') ||
                (outputKey.toLowerCase() === 'list' && typeof (outputValue) === 'string')) {
                editorOutput[outputKey] = getSubbandOutput(outputValue);                
            }  else if(['frequency', 'frequency_resolution', 'time_resolution'].indexOf(outputKey)>=0 ) {
                // Remove custom read only fields before passing the output back to the parent
                delete editorOutput[outputKey];
            }      
            else if (outputKey.toLowerCase() === 'duration') {
                // }  else if (durationProps.indexOf(outputKey)) {
                const splitOutput = outputValue.split(':');
                editorOutput[outputKey] = ((splitOutput[0] * 3600) + (splitOutput[1] * 60) + parseInt(splitOutput[2]));
            }          
            if (isDebugLoggingEnabled)  console.log("result of updateOutput for ",editorOutput);
        }
        return editorOutput;
    }

    /**
     * Function to format subband list inout arrived as Array to String
     * @param {Array} prpInput 
     */
    function getSubbandInput(prpInput) {
        let subbandString = "";
        for (let index = 0; index < prpInput.length; index++) {
            if (subbandString.length > 0) {
                subbandString += ",";
            }
            let firstVal = prpInput[index]
            let nextVal = prpInput[index];
            if (prpInput[index + 1] - nextVal === 1) {
                subbandString += firstVal + "..";
                while (prpInput[index + 1] - nextVal === 1) {
                    index++;
                    nextVal = prpInput[index];
                }
                subbandString += nextVal;
            } else {
                subbandString += firstVal;
            }
        }
        return subbandString;
    }

    /**
     * Convert time value in seconds to string format of HH:mm:ss
     * @param {Number} seconds 
     */
    function getTimeInput(seconds) {
        const hh = Math.floor(seconds / 3600);
        const mm = Math.floor((seconds - hh * 3600) / 60);
        const ss = +((seconds - (hh * 3600) - (mm * 60)) / 1);
        return (hh < 10 ? `0${hh}` : `${hh}`) + ':' + (mm < 10 ? `0${mm}` : `${mm}`) + ':' + (ss < 10 ? `0${ss}` : `${ss}`);
    }

    /**
     * Returns the frequency resolution value based on observationType or targetObservation props of the component
     * @param {Number} channelsPerSubband 
     * @param {Number} freqSteps
     * @returns Number - calculated value
     */
    function getFrequencyResolution(channelsPerSubband, freqSteps) {

        try{
            if (props.observationType?.indexOf('observation') >= 0) {
                return (clockValue / 1024 / channelsPerSubband * 1/1000).toFixed(2);
        }   else if (props.targetObservation) {
            const specification = props.targetObservation.specifications_doc;
            if (!specification) return null;
            return (clockValue / 1024 / (specification['correlator']?specification['correlator']['channels_per_subband']:specification['target']?.['correlator']?.['channels_per_subband']) * freqSteps / 1000).toFixed(2);
        }   else {
            return null;
        }
        }
        catch (e){
            let errornew =  new Error('getFrequencyResolution Error' + e?.message, {    context: {     props: props  ,channelsPerSubband:channelsPerSubband, freqSteps:freqSteps }});
            //faro?.api?.pushError(errornew);
            if (isDebugLoggingEnabled)  console.log("getFrequencyResolution Error",errornew)
            return null;
        }
        
    }

    /**
     * Returns calculated time resolution.
     * @param {Number} channelsPerSubband 
     * @param {Number} timeIntegrationSteps 
     * @returns Number calculated value
     */
    function getTimeResolution(channelsPerSubband, timeIntegrationSteps) {
        if (props.targetObservation && ( props.targetObservation.specifications_doc?.['correlator'] || props.targetObservation.specifications_doc['target'])) {
            return  ((props.targetObservation.specifications_doc?.['correlator'])?props.targetObservation.specifications_doc['correlator']['integration_time']
            :props.targetObservation.specifications_doc['target']?.['correlator']['integration_time']) * timeIntegrationSteps;
        }   else {
            return 1 / clockValue * 1024 * channelsPerSubband  * timeIntegrationSteps * (props.observationType==='beamforming observation'?1000:1);
        }
    }

    /**
     * Returns the frequency resolution value based on pulsar pipeline props of the component
     * @param {Number} channelsPerPart 
     * @param {Number} freqSteps
     * @returns Number - calculated value
     */
     function getPipelineFrequencyResolution(channelsPerPart, isDigifil) {
        if (props.observationType?.indexOf('pulsar') >= 0) {
            if (isDigifil) {
                return singlePulseSearch?(clockValue / 1024 / (channelsPerPart/props.targetObservation.specifications_doc.beamformer.pipelines[0].coherent.settings.subbands_per_file)).toFixed(2):'N/A';
            }   else {
                return filterBankEnabled?(clockValue / 1024 / (channelsPerPart/props.targetObservation.specifications_doc.beamformer.pipelines[0].coherent.settings.subbands_per_file)).toFixed(2):'N/A';
            }
        }   else {
            return 'N/A';
        }
    }

    /**
     * Returns calculated time resolution.
     * @param {Number} channelsPerPart 
     * @param {Number} timeIntegrationSteps 
     * @returns Number calculated value
     */
     function getPipelineTimeResolution(channelsPerPart, timeIntegrationSteps) {
        if (props.observationType?.indexOf('pulsar') >= 0) {
            return  singlePulseSearch?(1/channelsPerPart * props.targetObservation.specifications_doc.beamformer.pipelines[0].coherent.settings.time_integration_factor * timeIntegrationSteps):'N/A';
        }
    }




/**
     * Validates if the subband list custom field
     * @param {array} subbandArray 
     * * @param {String} bandpassFilter  , this is comming from the station_configuration.filter
     */
function getFrequencyFromSubbandOutput(subbandArray,bandpassFilter) {
    try {

            let frequencyValues = ""
            for (const subband of subbandArray) {
                const subbandRange = subband.split('..');
                if (subbandRange.length > 1) {
                    const firstVal = parseInt(subbandRange[0]);
                    const nextVal = parseInt(subbandRange[1]);
                    if ( nextVal <= 511 ) {
                        if (firstVal === 0 || firstVal < nextVal) {
                            if(frequencyValues) {
                                frequencyValues = frequencyValues + ',' + subband2frequencyRange(firstVal, nextVal, bandpassFilter);
                            } else {
                                frequencyValues = subband2frequencyRange(firstVal, nextVal, bandpassFilter);
                            }
                        } else {
                            return "";
                        }
                    } else {
                        return "";
                    }                        
                }
                 else {
                    if (isNaN(parseInt(subbandRange[0]))) {
                        return "";
                    }
                    if (parseInt(subbandRange[0]) < 0 || parseInt(subbandRange[0]) > 511) {
                        return "";
                    }
                    if(frequencyValues) {
                        frequencyValues = frequencyValues + ',' + subband2frequencyCenter(parseInt(subbandRange[0]), bandpassFilter).toString();
                    } else {
                        frequencyValues = subband2frequencyCenter(parseInt(subbandRange[0]), bandpassFilter).toString();
                    }
                }
            }
            return frequencyValues
    } catch (exception) {
        return "";
    }

}






    /**
     * Validates if the subband list custom field
     * @param {String} prpOutput 
     */
     function validateSubbandOutput(prpOutput, path) {
        try {
            if (prpOutput) {
                path = path?.split('.');
                if(path[path.length -1].toLowerCase() === 'subbands') path[path.length -1] = 'frequency';
                if(path[path.indexOf('list')]) path[path.indexOf('list')] = 'frequency';
                path = path.join('.')
                const subbandArray = prpOutput.split(",");
                let bandpassFilter = editorRef.current?.getValue()?.station_configuration ? (editorRef.current.getValue().station_configuration.filter || editorRef.current.getValue().station_configuration?.filter || null) : null;
                let frequencyValues = null
                for (const subband of subbandArray) {
                    const subbandRange = subband.split('..');
                    if (subbandRange.length > 1) {
                        const firstVal = parseInt(subbandRange[0]);
                        const nextVal = parseInt(subbandRange[1]);
                        if ( nextVal <= 511 ) {
                            if (firstVal === 0 || firstVal < nextVal) {
                                if(frequencyValues) {
                                    frequencyValues = frequencyValues + ',' + subband2frequencyRange(firstVal, nextVal, bandpassFilter);
                                } else {
                                    frequencyValues = subband2frequencyRange(firstVal, nextVal, bandpassFilter);
                                }
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }                        
                    }
                     else {
                        if (isNaN(parseInt(subbandRange[0]))) {
                            return false;
                        }
                        if (parseInt(subbandRange[0]) < 0 || parseInt(subbandRange[0]) > 511) {
                            return false;
                        }
                        if(frequencyValues) {
                            frequencyValues = frequencyValues + ',' + subband2frequencyCenter(parseInt(subbandRange[0]), bandpassFilter).toString();
                        } else {
                            frequencyValues = subband2frequencyCenter(parseInt(subbandRange[0]), bandpassFilter).toString();
                        }
                    }
                }
                if(editor?.editors[path]?.formname) { 
                    // add values to respective frequency
                    document.getElementById(editor.editors[path]?.formname).value = frequencyValues;
                    document.getElementById(editor.editors[path]?.formname).disabled = true;
                }
            } else {
                return false;
            }
        } catch (exception) {
            return exception.message === "JSON Editor not ready yet.  Listen for 'ready' event before getting the value";
        }
        return true;
    }

    /**
     * Function to calculate and update the readonly field frequency_resolution when channels_per_subband value is changed
     * @param {string} channelsPropPath - path of the channels_per_subband preoperty to get the id or name of the field
     * @param {number} channelsPerSubband - value selected in the dropdown
     * @param {number} freqSteps - value entered in the frequency steps of average property in pipeline task spec.
     */
    function updateFrequencyResolution(channelsPropPath, channelsPerSubband, freqSteps) {
        let freqResPath = channelsPropPath.split(".");
        freqResPath[freqResPath.length-1] = "frequency_resolution";
        freqResPath = freqResPath.join(".");
        if(editor.editors[freqResPath]?.formname) { 
            // Set values for Frequency Resolution field.
            document.getElementById(editor.editors[freqResPath]?.formname).value = getFrequencyResolution(channelsPerSubband, freqSteps);
        }
    }

    /**
     * Function to calculate and update the readonly field frequency_resolution when channels_per_part value is changed
     * @param {string} channelsPropPath - path of the channels_per_part preoperty to get the id or name of the field
     * @param {number} channelsPerPart - value selected in the dropdown
     * @param {boolean} isDigifil - ture if the it has digifil, false if it is filterbank
     */
     function updatePipelineFrequencyResolution(channelsPropPath, channelsPerPart, isDigifil) {
        if (!channelsPerPart) {
            let channelsPath = channelsPropPath.split(".");
            channelsPath[channelsPath.length-1] = 'channels_per_part';
            channelsPerPart = document.getElementById(editor.editors[channelsPath.join('.')]?.formname)?.value;
        }

        let freqResPath = channelsPropPath.split(".");
        freqResPath[freqResPath.length-1] = "frequency_resolution";
        freqResPath = freqResPath.join(".");
        let freqResolution = null;
        if(editor.editors[freqResPath]?.formname) { 
            // Set values for Frequency Resolution field.
            freqResolution = getPipelineFrequencyResolution(channelsPerPart, isDigifil);
            document.getElementById(editor.editors[freqResPath]?.formname).value = freqResolution!=='N/A'?(freqResolution/1000).toFixed(2):freqResolution;
        }
        return freqResolution;
    }

    /**
     * Add selected station values to station tag list
     */
    function addSelectedStationTags() {
        if (stationListPaths && stationListPaths.length > 0) {
            for (const stationListPath of stationListPaths ) {
                const path = stationListPath;
                const stationPath = stationListPath.replace(/]\[/g,".").replace("[",".").replace("]","");
                if (editor?.editors?.[stationPath]) {
                    let stationList =  editor.editors[stationPath].value;
                    let selectedStationsContent = '<label for="specification[resources][selected_station_list]">Selected Stations</label>';
                    selectedStationsContent += `<div id='${stationPath}.stations_tag' role='selected_stations'>`;
                    if (stationList && stationList.length>0) {
                        for (const station of stationList) {
                            selectedStationsContent += `<span class="tag-content p-chips-token-label selected-station-tag">${station}</span>`;
                        }
                    }   else {
                        //show message when no station selected
                        selectedStationsContent += '<span style="font-weight:normal"> - </span>';
                    }
                    selectedStationsContent += '</div>';
                    // Remove existing child selected station label and div
                    let stationListParentDiv = document.getElementsByName(path)[0].parentElement;
                    while (stationListParentDiv.childElementCount > 2) {
                        stationListParentDiv.removeChild(stationListParentDiv.lastChild);
                    }
                    // Append new children to display selected stations
                    let selectedStationsDiv = document.createElement('div');
                    stationListParentDiv.appendChild(selectedStationsDiv);
                    selectedStationsDiv.outerHTML = selectedStationsContent;
                }
            }
        }
    }

    /**
     * Function to calculate and update the readonly field time_resolution when channels_per_subband or integration time value is changed
     * @param {string} channelsPropPath - path of the channels_per_subband preoperty to get the id or name of the field
     * @param {number} channelsPerSubband - value selected in the dropdown, nullable 
     * @param {number} integrationTime - value selected in the dropdown, nullable
     */
     function updateTimeResolution(propPath, channelsPerSubband, integrationTime) {
        let timeResPath = propPath.split(".");
        timeResPath[timeResPath.length-1] = "time_resolution";
        timeResPath = timeResPath.join(".");
        if (!channelsPerSubband) {
            let channelsPath = propPath.split(".");
            channelsPath[channelsPath.length-1] = 'channels_per_subband';
            channelsPerSubband = document.getElementById(editor.editors[channelsPath.join('.')]?.formname)?.value;
        }
        if (!integrationTime) {
            let intTimePath = propPath.split(".");
            intTimePath[intTimePath.length-1] = props.observationType==='beamforming observation'?'time_integration_factor':'integration_time';
            integrationTime = document.getElementById(editor.editors[intTimePath.join('.')]?.formname)?.value;
        }
        if(editor.editors[timeResPath]?.formname) { 
            // Set values for Time Resolution field.
            document.getElementById(editor.editors[timeResPath]?.formname).value = getTimeResolution(channelsPerSubband, integrationTime);
        }
    }

    /**
     * Function to calculate and update the readonly field time_resolution when channels_per_part or integration time value is changed
     * @param {string} channelsPropPath - path of the channels_per_part preoperty to get the id or name of the field
     * @param {number} channelsPerPart - value selected in the dropdown, nullable 
     * @param {number} integrationTime - value selected in the dropdown, nullable
     */
     function updatePipelineTimeResolution(propPath, channelsPerPart, integrationTime) {
        let timeResPath = propPath.split(".");
        timeResPath[timeResPath.length-1] = "time_resolution";
        timeResPath = timeResPath.join(".");
        if (!channelsPerPart) {
            let channelsPath = propPath.split(".");
            channelsPath[channelsPath.length-1] = 'channels_per_part';
            channelsPerPart = document.getElementById(editor.editors[channelsPath.join('.')]?.formname)?.value;
        }
        if (!integrationTime) {
            let intTimePath = propPath.split(".");
            intTimePath[intTimePath.length-1] = props.observationType==='pulsar pipeline'?'integration_time_factor':'integration_time';
            integrationTime = document.getElementById(editor.editors[intTimePath.join('.')]?.formname)?.value;
        }
        if(editor.editors[timeResPath]?.formname) { 
            // Set values for Time Resolution field.
            const timeResolution = getPipelineTimeResolution(channelsPerPart, integrationTime);
            document.getElementById(editor.editors[timeResPath]?.formname).value = timeResolution!=='N/A'?(timeResolution*1000).toFixed(5):timeResolution;
        }
    }

    /**
     * Convert the string input for subband list to Array
     * @param {String} prpOutput 
     */
    function getSubbandOutput(prpOutput) {
        const subbandArray = prpOutput ? prpOutput.split(",") : [];
        let subbandList = [];
        for (const subband of subbandArray) {
            const subbandRange = subband.split('..');
            if (subbandRange.length > 1) {
                subbandList = subbandList.concat(_.range(subbandRange[0], (parseInt(subbandRange[1]) + 1)));
            } else {
                subbandList = subbandList.concat(parseInt(subbandRange[0]));
            }
        }
        prpOutput = subbandList;
        return prpOutput;
    }

    return (
        
            <div className='jsonspeceditor'>
            {/* Loader to show while resolving external references */}
            <div id={props.id ? `${props.id}_loader` : 'editor_holder_loader'}>
            </div>
            <div id={props.id ? props.id : 'editor_holder'}></div>
            </div>
    );
}

export default Jeditor;

Jeditor.propTypes = {
  schema: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.oneOf([null, undefined])]),
  bandPassFilter: PropTypes.string,
  loader: PropTypes.any,
  id: PropTypes.string,
  isDebugLoggingEnabled: PropTypes.bool,
  resolveExtRef: PropTypes.bool,
  showInGrid: PropTypes.bool,
  title: PropTypes.string,
  errorsOn: PropTypes.bool,
  initValue: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.oneOf([null, undefined])]),
  disabled: PropTypes.bool,
  parentFunction: PropTypes.func,
  observationType: PropTypes.string,
  fieldsToDisable: PropTypes.array,
  formatOutput: PropTypes.bool,
  Theme:PropTypes.string,
  callback: PropTypes.func,
  targetObservation: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.oneOf([null, undefined])]),
  systemTime: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.oneOf([null, undefined])])
}
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import {MultiSelect} from 'primereact/multiselect';
import { OverlayPanel } from 'primereact/overlaypanel';
import {InputText} from 'primereact/inputtext';
import { Button } from 'primereact/button';

import  Station from "../utils/station"
import _ from 'lodash';

const Stations = (props) => {
    let op;

    const [selectedStations, setSelectedStations] = useState([]);
    const [stationOptions, setStationOptions] = useState([]);
    const [customStations, setCustomStations] = useState([]);
    const [customStationsOptions, setCustomStationsOptions] = useState([]);
    const [stations, setStations] = useState([]);
    const [missing_StationFieldsErrors, setMissing_StationFieldsErrors] = useState([]);
    const [state, setState] = useState({
        Custom: {
            stations: []
        }
    });

       /**
     * Cosntruct and set appropriate values to each station by finding station from station_group
     * like error, missing fields, etc.
     * Also will construct stations for custom group by merging all the stations
     */
       const getStationsDetails = (stations, responses) => {
        let stationState = {
            Custom: {
                stations: []  
            }
        };
        let custom_Stations = [];
        setStationOptions(stations);
        let selected_Stations = [];
        responses.forEach((response, index) => {
            const StationName = stations[index].value;
            let missing_StationFields;
            try{
                let stationGroup = '';
                if (props.stationGroup[props.taskName]) {
                    stationGroup = props.stationGroup[props.taskName];
                }   else {
                    stationGroup = props.stationGroup;
                }
                missing_StationFields = stationGroup.find(i => {
                    if (i.stations.length === response.stations.length && i.stations[0] === response.stations[0]) {
                        i.stationType = StationName;
                        return true;
                    }
                    return false;
                });
            }catch(err) { /* empty */ }
            // Missing fields present then it matched with station type otherwise its a custom...
            if (missing_StationFields) {
                selected_Stations = [...selected_Stations, StationName];
            }
            stationState ={
                ...stationState,
                [StationName]: {
                    stations: response.stations,
                    max_nr_missing: missing_StationFields ? isNaN(missing_StationFields.max_nr_missing)? response.max_nr_missing: missing_StationFields.max_nr_missing : response.max_nr_missing
                    
                },
                Custom: {
                    stations: [...stationState['Custom'].stations, ...response.stations], 
                },
            };
            // Setting in Set to avoid duplicate, otherwise have to loop multiple times.
            custom_Stations = new Set([...custom_Stations, ...response.stations]);
        });
        // Find the custom one
        let stationGroup = '';
        if (props.stationGroup[props.taskName]) {
            stationGroup = props.stationGroup[props.taskName];
        }   else {
            stationGroup = props.stationGroup;
        }
        const custom_stations = stationGroup.filter(i => !i.stationType);
        stationState = {
            ...stationState
        };
        // Stationtype is not a valid element in the document definition.
        stationGroup.forEach(station => {
            if (station.stationType) {
              delete station.stationType;
            }
          });
          
        setCustomStations(custom_stations);
        setSelectedStationGroup(_.cloneDeep(selected_Stations));
        setState(stationState);        
        let custom_stations_options = Array.from(custom_Stations);
        custom_stations_options = _.sortBy(custom_stations_options);
        // Changing array of sting into array of objects to support filter in primereact multiselect
        custom_stations_options = custom_stations_options.map(i => ({ value: i })); 

        setCustomStationsOptions(custom_stations_options);
        if (props.onUpdateStations) {
            updateStationsInSchedulingComp(stationState, _.cloneDeep(selected_Stations), missing_StationFieldsErrors, custom_stations);
        }
    };


    const getAllStations = async () => {
        const stationGroup =  Station.getStationGroup(props.commonSchemaTemplate);
        let response = [];
        stationGroup.forEach(st => {
            let stations = Station.getStations(props.commonSchemaTemplate,st.value)
            response.push(stations);
            
        });
        getStationsDetails(stationGroup, response);
        setStationOptions(stationGroup);
    };

  useEffect(() => {
            reset();
            getAllStations();
           
    }, [props.stationGroup]); // eslint-disable-line react-hooks/exhaustive-deps



    // Restting the stations
    const reset = () => {
        setStations([]);
        setSelectedStations([]);
        setStationOptions([]);
        setCustomStations([]);
        setCustomStationsOptions([]);
        setMissing_StationFieldsErrors([]);
    };




    /**
     * Method will trigger on change of station group multiselect.
     * Same timw will update the parent component also
     * *param value* -> array of string 
     */
    const setSelectedStationGroup = (value) => {
        //Show only All in station group if 'All' selected
        if (_.includes(value, 'All')) {
            value = ['All'];
        }
        setSelectedStations(value);
        if (props.onUpdateStations) {
            updateStationsInSchedulingComp(state, value, missing_StationFieldsErrors, customStations);
        }
    };

    /**
     * Method will trigger on change of custom station dropdown.
     */
    const onChangeCustomSelectedStations = (value, index) => {
        const custom_selected_options = [...customStations];
        custom_selected_options[index].stations = value;
        if (value < custom_selected_options[index].max_nr_missing || !value.length) {
            custom_selected_options[index].error = true;
        } else {
            custom_selected_options[index].error = false;
        }
        setCustomStations(custom_selected_options);
        updateStationsInSchedulingComp(state, selectedStations, missing_StationFieldsErrors, custom_selected_options);
    };

    /**
     * Method will trigger on click of info icon to show overlay
     * param event -> htmlevent object
     * param key -> string - selected station
     */
    const showStations = (event, key) => {
        op.toggle(event);
        setStations((state[key] && state[key].stations ) || []);
    };

    /**
     * Method will trigger on change of missing fields.
     * Will store all fields error in array of string to enable/disable save button.
     */
    const setNoOfmissing_StationFields = (key, value) => {
        let cpmissing_StationFieldsErrors = [...missing_StationFieldsErrors];
        if (isNaN(value) || value > state[key].stations.length || value.trim() === '') {
            if (!cpmissing_StationFieldsErrors.includes(key)) {
                cpmissing_StationFieldsErrors.push(key);
            }
        } else {
            cpmissing_StationFieldsErrors = cpmissing_StationFieldsErrors.filter(i => i !== key);
        }
        setMissing_StationFieldsErrors(cpmissing_StationFieldsErrors);
        const stationState = {
            ...state,
            [key]: {
                ...state[key],
                max_nr_missing: value,
                error: isNaN(value) || value > state[key].stations.length || value.trim() === ''
            },
        };
        setState(stationState);
        if (props.onUpdateStations) {
            updateStationsInSchedulingComp(stationState, selectedStations, cpmissing_StationFieldsErrors, customStations);
        }
    }

    /**
     * Method will trigger onchange of missing fields in custom
     * @param {*} value string
     * @param {*} index number
     */
    const setMissingFieldsForCustom = (value, index) => {
        const custom_selected_options = [...customStations];
        if (isNaN(value) || value > custom_selected_options[index].stations.length || value.trim() === '' || !custom_selected_options[index].stations.length) {
            custom_selected_options[index].error = true;
        } else {
            custom_selected_options[index].error = false;
        }
        custom_selected_options[index].touched = true;
        custom_selected_options[index].max_nr_missing = value;
        setCustomStations(custom_selected_options);
        updateStationsInSchedulingComp(state, selectedStations, missing_StationFieldsErrors, custom_selected_options);
    };

    /**
     * Method will get trigger on click of add custom
     */
    const addCustom = async() => {
        const custom_selected_options = [...customStations];
        const commonSchemaTemplate = props.commonSchemaTemplate;
        const stationGroups = commonSchemaTemplate.ref_resolved_schema.definitions.station_group.anyOf;
        const customStation = _.find(stationGroups, {title: 'Custom'});
        custom_selected_options.push({
            stations: customStation.default.stations,
            max_nr_missing: customStation.default.max_nr_missing,
            error: true
        });
        setCustomStations(custom_selected_options);
        updateStationsInSchedulingComp(state, selectedStations, missing_StationFieldsErrors, custom_selected_options);
    };

    const updateStationsInSchedulingComp = (param_State, param_SelectedStations, param_missing_StationFieldsErrors, param_Custom_selected_options) => {
        const isError = param_missing_StationFieldsErrors.length || param_Custom_selected_options.filter(i => i.error).length;

        props.onUpdateStations(param_State, param_SelectedStations, isError, param_Custom_selected_options, props.taskName);
    };
    
    /**
     * Method to remove the custom stations
     * @param {*} index number
     */
    const removeCustomStations = (index) => {
        const custom_selected_options = [...customStations];
        custom_selected_options.splice(index,1);
        setCustomStations(custom_selected_options);
        updateStationsInSchedulingComp(state, selectedStations, missing_StationFieldsErrors, custom_selected_options);
    };
 
    return (
        <div className={`p-field p-grid grouping p-fluid ${props.isSummary && 'p-col-12'}`} style={{height: props.height}}>
            <fieldset>
                <legend>
                    <label>{props.taskName? props.taskName : 'Station Groups'}<span style={{color:'red'}}>*</span></label>
                </legend>
                {!props.isSummary && <>
                    {!props.view && <div className="col-sm-12 p-field p-grid" data-testid={`${props.taskName}_stations`}>
                        <div className="col-md-6 d-flex">
                            <label htmlFor="stationgroup" className="col-sm-6 station_header">Station Groups</label>
                            <div className="col-sm-6">
                                <MultiSelect data-testid="stations" optionLabel="value" optionValue="value" filter={true} style={{minWidth:"400px","maxWidth": "400px"}}
                                    value={selectedStations} 
                                    options={stationOptions} 
                                    placeholder="Select Stations"
                                    onChange={(e) => setSelectedStationGroup(e.value)}
                                    id="station-group-select"
                                />
                            </div>
                        </div>
                        <div className="add-custom">
                            <Button onClick={addCustom} label="Add custom stations" icon="pi pi-plus" disabled={!stationOptions.length} id="add-custom-button" />
                        </div>
                    </div>}
                    {selectedStations.length || customStations.length ? <div className="col-sm-12 selected_stations" data-testid="selected_stations">
                        {<div className="col-sm-12"><label style={{paddingLeft: '8px'}}>Maximum number of stations that can be missed in the selected groups</label></div>}
                        <div className="col-sm-12 p-0 d-flex flex-wrap">
                            {selectedStations.map((i,index) => ( 
                                    <div className="p-field p-grid col-md-6" key={i}>
                                        <label className="col-sm-6 text-caps">
                                            {i}&nbsp;
                                            <i className="pi pi-info-circle info label-icon" onClick={(e) => showStations(e, i)} />
                                        </label>
                                        <div className="col-sm-6">
                                            <InputText id={`sg_missingstation_${index}`} data-testid={`sg_missingstation_${index}`} name="sg_missingstation" style={{minWidth:"50px","maxWidth": "50px"}}
                                                className={(state[i] && state[i].error) ?'input-error':''}
                                                 maxLength="128"
                                                placeholder="Max Number of Missing Stations"
                                                value={state[i] ? (state[i].max_nr_missing) : ''}
                                                disabled={props.view}
                                                onChange={(e) => setNoOfmissing_StationFields(i, e.target.value)}/>
                                            {(state[i] && state[i].error) && <span className="error-message">{state[i].max_nr_missing ? `Max. no of missing stations is ${state[i] ? state[i]?.stations?.length : 0}` : 'Max. no of missing stations required'}</span>}
                                        </div>
                                    </div>
                                ))}
                                {customStations.map((stat, index) => (
                                    <div className="p-field p-grid col-md-12 custom-station-wrapper" key={index}>
                                        {!props.view && <Button icon="pi pi-trash" id={`custom-remove-${index}`} className="p-button-secondary p-button-text custom-remove" onClick={() => removeCustomStations(index)} />}


                                        <div className="col-md-6 p-field p-grid custom-station-wrapper-station-select">
                                            <label className="col-sm-6 text-caps custom-label" style={{minWidth:"150px","maxWidth": "150px"}}>
                                            {index + 1}. Custom Stations 
                                            </label>
                                            <div className="col-sm-6 pr-8 custom-value">
                                                <MultiSelect data-testid="custom_stations" id={`custom_stations_${index}`} style={{minWidth:"400px","maxWidth": "400px"}}
                                                    className='custom_stations' filter
                                                    value={stat.stations} 
                                                    options={customStationsOptions} 
                                                    placeholder="Select Stations"
                                                    disabled={props.view}
                                                    optionLabel="value"
                                                    optionValue="value" 
                                                    onChange={(e) => onChangeCustomSelectedStations(e.value, index)}
                                                />
                                            </div>
                                        </div>

                                        
                                        <div className="col-md-6 p-field p-grid">
                                            <label className="col-sm-6 customMissingStationLabel"  style={{minWidth:"250px","maxWidth": "250px"}}>
                                                Maximum Number of missing stations
                                            </label>
                                        <div className="col-sm-6 pr-8 custom-field">
                                            <InputText id={`missingStation-${index}`} data-testid={`missingStation-${index}`}   style={{minWidth:"50px","maxWidth": "50px"}}
                                                className={`${(stat.error && stat.touched) ?'input-error':''} missing_station`}
                                                placeholder="Max Number of Missing Stations"
                                                value={stat.max_nr_missing}
                                                disabled={props.view}
                                                onChange={(e) => setMissingFieldsForCustom(e.target.value, index)}/>
                                                {(stat.error && stat.touched) && <span className="error-message">{stat.max_nr_missing ? `Max. no of missing stations is ${stat.stations.length}` : 'Max. no of missing stations required'}</span>}
                                        
                                        </div>
                                        </div>

                                        
                                    </div>
                                ))}
                        </div>
                        
                    </div> : null}
                </>}
                {/* For timeline view, displaying all stations in list */}
                {props.isSummary && (
                    <div className="list-stations-summary">
                        {state && Object.keys(state).map(key => {
                            if (key !== 'Custom') {
                                return (
                                    <>
                                        {state[key].stations.map((station) => <div key={station}>{station}</div>)}
                                    </>
                                )
                            }
                            return <></>
                        })}
                         {customStations.map((stat) => (
                             <>
                                {stat.stations.map(station => <div key={station}>{station}</div>)}
                             </>
                         ))}
                    </div>
                )}
                <OverlayPanel ref={(el) => op = el} dismissable  style={{width: '200px'}} className="overlay-panel">
                    <h6 className="overlay-panel-header">Stations in group</h6>
                    <div className="station-container">
                        {(stations || []).map(i => (
                            <span key={i}>{i}</span>
                        ))}
                    </div>
                </OverlayPanel>
            </fieldset>
        </div>
    );
};

Stations.propTypes = {
  stationGroup: PropTypes.any,
  taskName: PropTypes.any,
  onUpdateStations: PropTypes.func,
  isSummary: PropTypes.string,
  height: PropTypes.any,
  view: PropTypes.any,
  commonSchemaTemplate:PropTypes.object
}

export default Stations;


import { useState, useEffect, useRef } from "react";

import {
  SchedulingUnitObservingStrategy,
  TaskTemplate,
  SchedulingConstraintsTemplate,
  CommonSchemaTemplate,
  TaskParamSchema,
  TaskDraftList,
  EditorError,
  MultiStationConfig,
  CustomgStationCheck,
  MissingStationCheck,
  StationGroupErrorState,
  StationsData,
  StationsDataCustom,
  TemplateDefinition,
  SpecificationDetailsFormStatus
} from "@astron-sd/telescope-specification-models";
import {
  SchedulingConstraints,
  UnitConverter,
  getConstraintTemplate,
  Stations,
  Jeditor,
  getTaskTemplateForTask,
  ParserUtility,
  prepareUpdateConstraintsParameters,
} from "../../lib/main";

import "flatpickr/dist/flatpickr.css";
import _ from "lodash";


type Props = {
  definition?: TemplateDefinition;
  definitionCallback: (definition: TemplateDefinition, status: SpecificationDetailsFormStatus) => void;
  constraintSchemaCallback?: (constraintSchema: SchedulingConstraintsTemplate) => void;
  strategy?: SchedulingUnitObservingStrategy;
  commonSchemaTemplate?: CommonSchemaTemplate;
  taskTemplates?: TaskTemplate[];
  taskFilters?: TaskDraftList;
  constraintTemplates?: SchedulingConstraintsTemplate[];
  systemUTC?: string;
  isDebugLoggingEnabled?: boolean;
  disable: boolean;
  theme?:string
  paramsOutput?:  {[key: string]: unknown };
};
interface Time {
  at: string;
}

interface ConstraintParams {
  scheduler: string;
  time: Time;
}

const SpecificationDetailsForm = (props: Props) => {
  const isDebugLoggingEnabled = props.isDebugLoggingEnabled;
  if (isDebugLoggingEnabled) console.log("----------------- \\ SpecificationDetailsForm props", props);
  const isNew = !props.definition;
  
  const [validConstraints, setValidConstraints] = useState(true);
  const [selectedStationsOfTask, setSelectedStationsOfTask] = useState<MultiStationConfig>();
  const [customStationsOfTask, setCustomStationsOfTask] = useState<CustomgStationCheck>();
  const [missingStationsofTask, setMissingStationsofTask] = useState<MissingStationCheck>();
  const [missingStnErrorsofTask, setMissingStnErrorsofTask] = useState<StationGroupErrorState>();
  const [observationType, setObservationType] = useState<string | undefined>();
  const [bandPassFilter, setBandPassFilter] = useState<string | undefined>();
  const [paramsSchema, setParamsSchema] = useState<TaskParamSchema | undefined>();
  const [paramsOutput, setParamsOutput] = useState<{ [key: string]: unknown } | undefined>();
  const theme :string = props.theme?.length>0?  props.theme  : "tailwind";
  const [constraintParamsOutput, setConstraintParamsOutput] = useState<{ [key: string]: unknown } | undefined>();

  const [stationGroups, setStationGroups] = useState<{ [key: string]: unknown } | undefined>();
  const isValidTaskEditor  = useRef(!isNew);
  const isValidConstraintsEditor = useRef(!isNew);

  if (isDebugLoggingEnabled) console.log("Received Contraint", _.cloneDeep(props.definition?.scheduling_constraints_doc));
  const preparedConstraint = _.cloneDeep(props.definition?.scheduling_constraints_doc);
  const preparedParamsOutput = _.cloneDeep(props.definition);
  const [updatedConstraintParams,setUpdatedConstraintParams] = useState<TemplateDefinition>();
  const updatedParamsOutput  = useRef(preparedParamsOutput);
  if (isDebugLoggingEnabled) console.log("preparedParamsOutput", _.cloneDeep(preparedParamsOutput));
  

  const [taskErrors, setTaskErrors] = useState<EditorError[]>();
  const [constraintSchema, setConstraintSchema] = useState<SchedulingConstraintsTemplate>();

  useEffect(() => {


    const setStrategyConstraint = (name: string, version: string) => {
      if (!props.strategy?.template?.scheduling_constraints_template?.name) return;
      if (isDebugLoggingEnabled) console.log("requesting constraint" + name + " version:" + version, props.constraintTemplates,props.strategy);
      const strategyConstraint: SchedulingConstraintsTemplate = getConstraintTemplate(
        props.constraintTemplates,
        name,
        version,
      );
      if (strategyConstraint) {
        strategyConstraint.ref_resolved_schema.properties.sky.properties.transit_offset.properties.from.default =
          UnitConverter.getSecsToHHmmssWithSign(
            strategyConstraint.ref_resolved_schema.properties.sky.properties.transit_offset.properties.from.default,
          );
        strategyConstraint.ref_resolved_schema.properties.sky.properties.transit_offset.properties.to.default =
          UnitConverter.getSecsToHHmmssWithSign(
            strategyConstraint.ref_resolved_schema.properties.sky.properties.transit_offset.properties.to.default,
          );
        
        if (strategyConstraint?.ref_resolved_schema?.properties?.sky?.properties?.min_distance?.properties?.sun) {
            strategyConstraint.ref_resolved_schema.properties.sky.properties.min_distance.properties.sun.propertyOrder = 1;
        }
        
        if (strategyConstraint?.ref_resolved_schema?.properties?.sky?.properties?.min_distance?.properties?.moon) {
            strategyConstraint.ref_resolved_schema.properties.sky.properties.min_distance.properties.moon.propertyOrder = 2;
        }
        
        if (strategyConstraint?.ref_resolved_schema?.properties?.sky?.properties?.min_distance?.properties?.jupiter) {
            strategyConstraint.ref_resolved_schema.properties.sky.properties.min_distance.properties.jupiter.propertyOrder = 3;
        }
        
        setConstraintSchema(_.cloneDeep(strategyConstraint));
        if (props.constraintSchemaCallback) props.constraintSchemaCallback(strategyConstraint);
      };
    }

    if (props.constraintTemplates?.length == 1) {
      setConstraintSchema(props.constraintTemplates[0])
    };


    setStrategyConstraint(
      props.strategy?.template?.scheduling_constraints_template?.name,
      props.strategy?.template?.scheduling_constraints_template?.version || "",
    );

  }, [props.strategy]); // eslint-disable-line react-hooks/exhaustive-deps
  
  // This will be broken into multiple parts and possible split back to shared component
  useEffect(() => {
    const PrepareTasks = async () => {
      if (isDebugLoggingEnabled) console.log("\\ Prepare Tasks",props.strategy?.template?.tasks,constraintSchema?.ref_resolved_schema?.["definitions"])
      if (!props.strategy?.template?.tasks)      {
        if (isDebugLoggingEnabled) console.log(" No Tasks, Returning !!");
        return
      }
      
      if (!constraintSchema?.ref_resolved_schema?.["definitions"]) 
      {
        if (isDebugLoggingEnabled) console.log("props.definition, but no constraint, Returning !!",props.definition,constraintSchema);
        return;
      }
      if (isDebugLoggingEnabled) console.log("Prepare Tasks - Passed Checks")
      const paramsOutput: { [key: string]: unknown } = {};
      const tasks = props.strategy.template.tasks;
      const parameters = props.strategy.template.parameters;
      const stationGroups: { [key: string]: unknown } = {}; // Correct type annotation
      const schema: TaskParamSchema = {
        type: "object",
        additionalProperties: false,
        properties: {},
        definitions: {},
      };

      for (const param of parameters) {
        if (!param.refs[0].startsWith("#/tasks/")) {
          continue;
        }
        let taskPaths = param.refs[0].split("/");
        const taskName = taskPaths[2];
        taskPaths = taskPaths.slice(taskPaths.length === 4 ? 3 : 4, taskPaths.length);
        const task = tasks[taskName];
        if (task) {
          if (taskPaths.includes("station_groups")) {
            stationGroups[param.name] = ParserUtility.getValueFromJsonPointer(
              props.strategy.template as unknown,
              param.refs[0],
            );
          } else {
            const taskTemplate = getTaskTemplateForTask(props.taskTemplates, task);

            if (["target observation", "beamforming observation"].indexOf(taskTemplate.name.toLowerCase()) >= 0) {
              setObservationType(taskTemplate.name);
            }

            // Get the default Bandpass filter and pass to the editor for frequency calculation from subband list
            if (taskTemplate.type_value === "observation" && task.specifications_doc.station_configuration?.filter) {
              setBandPassFilter((task.specifications_doc.station_configuration?.filter) as any);
            } else if (taskTemplate.type_value === "observation" && taskTemplate.ref_resolved_schema.definitions.filter) {
              setBandPassFilter(taskTemplate.ref_resolved_schema.definitions.filter.default);
            }


            schema.definitions = _.merge(
              constraintSchema?.ref_resolved_schema?.["definitions"],
              schema.definitions,
              taskTemplate?.ref_resolved_schema.definitions);
            
            taskPaths.reverse();
            const paramProp = await ParserUtility.getParamPropertyV2(
              taskPaths,
              taskTemplate.ref_resolved_schema,
              props.taskFilters,
            );
            schema.properties[param.name] = { ...paramProp };
            if (schema.properties[param.name]) {
              schema.properties[param.name].title = param.name;
              try {

                schema.properties[param.name].default = ParserUtility.getValueFromJsonPointer(
                  props.definition,
                  param.refs[0],
                );

                if (schema.properties[param.name].default === undefined) {
                  schema.properties[param.name].default = ParserUtility.getValueFromJsonPointer(
                    props.strategy.template,
                    param.refs[0],
                  );
                }
              } catch (err) {
                /* empty */
              }
              const defaultparam = ParserUtility.getDefaultParamValue(schema.properties[param.name]?.type ?? "");
              paramsOutput[param.name] = schema.properties[param.name]?.default || defaultparam;
            }
          }
        }
      }

      let defaultparams = ParserUtility.extractdefaultsFromConstraint(constraintSchema.ref_resolved_schema)

      let newvalue =updatedConstraintParams;
      let merged = _.merge(defaultparams,props.strategy.template.scheduling_constraints_doc,props.definition?.scheduling_constraints_doc,newvalue);
      
      setUpdatedConstraintParams(merged);
            
      
      if (isDebugLoggingEnabled) console.log("-->Here above we have set the UpdatedContstraintsParameters from the strategy, being ",props.strategy.template.scheduling_constraints_doc)
      setStationGroups(stationGroups);
      setParamsSchema(schema);
      if (!props.disable  ||  !paramsOutput) {
        if (isDebugLoggingEnabled) console.log(" paramsoutput is now ", paramsOutput, "from defintion", props.definition,props.paramsOutput)
        const blender = { ...paramsOutput, ...props.strategy.template.scheduling_constraints_doc}
          if (isDebugLoggingEnabled) console.log(" i am setting paramsoutput to", blender, "from defintion", props.definition,props.paramsOutput)
          setParamsOutput(_.cloneDeep(paramsOutput));
      }
      else 
      {
        if (isDebugLoggingEnabled) console.log(" i am setting paramsoutput bluntly from props")
        if (props.paramsOutput && Object.keys(props.paramsOutput).length>0)    setParamsOutput(props.paramsOutput);
        if (isDebugLoggingEnabled) console.log(" i am leave  paramsoutput as it is", paramsOutput, "from incomming",props.paramsOutput)
      }
      
    };
    PrepareTasks();
  }, [props.strategy, props.taskTemplates, props.taskFilters,constraintSchema]);

  const editorFunction = () => {
    if (isDebugLoggingEnabled) console.log("editorFunction called");
  };

  const makeCallBackFordUpdatedDefinition = (
     updatingCustomStationsOfTask = customStationsOfTask,
     updatingSelectedStationsOfTask = selectedStationsOfTask,
     updatingMissingStationsofTask = missingStationsofTask, 
     updatingdConstraintParams = updatedConstraintParams,
     updatingTasksParamsOutput = updatedParamsOutput.current,
     updatingIsValidConstraintsEditor = isValidConstraintsEditor.current,
     updatingIsValidTaskEditor = isValidTaskEditor.current
    ) => {
      
    const updatedDefinition = getUpdatedDefinition(updatingCustomStationsOfTask, updatingSelectedStationsOfTask, updatingMissingStationsofTask, updatingdConstraintParams, updatingTasksParamsOutput);
    if (!props.definitionCallback ) return;
    props.definitionCallback(updatedDefinition, {
      isValidConstraintsEditor: updatingIsValidConstraintsEditor,
      isValidTaskEditor: updatingIsValidTaskEditor,
      taskErrors: taskErrors,
    });
    return updatedDefinition;
  };

  const handleTaskCallBack = (jsonOutput: { [key: string]: unknown }, errors: EditorError[]) => {
    if (!props.disable)   setParamsOutput(_.cloneDeep(jsonOutput));
    updatedParamsOutput.current = _.cloneDeep(jsonOutput)
    const isValid =errors.length === 0
    if (isDebugLoggingEnabled) console.log("- handleTaskCallBack isvalid Now :" + isValid , errors,jsonOutput);
    isValidTaskEditor.current =isValid;
    setTaskErrors(errors);
    let newcombined = makeCallBackFordUpdatedDefinition(undefined, undefined, undefined, undefined, jsonOutput,undefined,isValid);
    
  };

  const getUpdatedDefinition  = (updatingCustomStationsOfTask = customStationsOfTask, updatingSelectedStationsOfTask = selectedStationsOfTask, updatingMissingStationsofTask = missingStationsofTask, updatingdConstraintParams = updatedConstraintParams, updatingTasksParamsOutput = updatedParamsOutput.current) => {
    const updatedDefinition = ParserUtility.reparseParameters(
      props.strategy,
      selectedStationsOfTask,
      missingStationsofTask,
      customStationsOfTask,
      updatingTasksParamsOutput
    );
    const observStrategy = _.cloneDeep(updatedDefinition);
    let newSpecificationDoc: { [key: string]: unknown } = {};
    if (observStrategy?.template) {
      for (const parameter of observStrategy.template.parameters) {
        for (const ref of parameter.refs) {
          let value = ParserUtility.getValueFromJsonPointer(updatedDefinition, ref);

          if (value !== undefined) {
            newSpecificationDoc = ParserUtility.setValueFromJsonPointer(newSpecificationDoc, ref, value);
          } else {
            value = ParserUtility.getValueFromJsonPointer(observStrategy.template, ref);
            if (value !== undefined) {
              newSpecificationDoc = ParserUtility.setValueFromJsonPointer(newSpecificationDoc, ref, value);
            }
          }

        }
      }
    }

    if (updatingdConstraintParams) {
      const copy = _.cloneDeep(updatingdConstraintParams) as unknown  as ConstraintParams;
      if (copy) { //scheduling_constraints_doc
        if ((copy.scheduler === "dynamic"  || copy.scheduler === "online" )&& copy.time?.at === "") {
          delete copy.time.at ;
          if (isDebugLoggingEnabled) console.log("deleteing time at",copy)
       } else
       {
        console.log("did not delete time at",copy)
       }
        newSpecificationDoc["scheduling_constraints_doc"] = copy;
      }
    }
    if (isDebugLoggingEnabled) console.log("Updated Definition", newSpecificationDoc, paramsOutput,updatingTasksParamsOutput)
    return newSpecificationDoc;
  };

  const onUpdateStations = (
    stationState: StationsData,
    selectedStations: Array<string>,
    missing_StationFieldsErrors: number,
    customSelectedStations: StationsDataCustom,
    taskName: string,
  ) => {

    const updatingSelectedStationsOfTask = selectedStationsOfTask || {};
    const updatingCustomStationsOfTask = customStationsOfTask || {};
    const updatingMissingStationsofTask = missingStationsofTask || {};
    const updatingMissingStnErrorsofTask = missingStnErrorsofTask || {};
    updatingMissingStnErrorsofTask[taskName] = missing_StationFieldsErrors || 0;
    updatingSelectedStationsOfTask[taskName] = selectedStations || [];
    updatingCustomStationsOfTask[taskName] = customSelectedStations || {};
    updatingMissingStationsofTask[taskName] = stationState || {};

    if (isDebugLoggingEnabled) {
      console.log("onUpdateStations!");
      console.log("taskname: " + taskName);
      console.log("stationState: ", stationState);
      console.log("selectedStations: ", selectedStations);
      console.log("missing_StationFieldsErrors: ", missing_StationFieldsErrors);
      console.log("customSelectedStations: ", customSelectedStations);
    }

    setCustomStationsOfTask(updatingCustomStationsOfTask);
    setSelectedStationsOfTask(updatingSelectedStationsOfTask);
    setMissingStationsofTask(updatingMissingStationsofTask);
    setMissingStnErrorsofTask(updatingMissingStnErrorsofTask);
    makeCallBackFordUpdatedDefinition(updatingCustomStationsOfTask, updatingSelectedStationsOfTask, updatingMissingStationsofTask);
  };

  const getTime = async () => {
    return props.systemUTC;
  };

  const handleConstraintsCallBack = (jsonOutput: { [key: string]: unknown }, errors: EditorError[]) => {

    if (isDebugLoggingEnabled) console.log("   -  handleConstraintsCallBackonstraintsCallBack errors", errors);
    if (isDebugLoggingEnabled) console.log("   -  handleConstraintsCallBackonstraintsCallBack jsonOutput", jsonOutput);
    if (isDebugLoggingEnabled) console.log("   -  handleConstraintsCallBackonstraintsCallBack paramsOutput",paramsOutput);
    if (isDebugLoggingEnabled) console.log("   -  handleConstraintsCallBackonstraintsCallBack updatedParamsOutput", updatedParamsOutput.current);
    if (isDebugLoggingEnabled) console.log("   -  handleConstraintsCallBackonstraintsCallBack updatedconstraint ", updatedConstraintParams);

    let newcombined = {...jsonOutput,...paramsOutput}
    if (isDebugLoggingEnabled) console.log("\\ handleConstraintsCallBackonstraintsCallBack newcombined ", newcombined);
    // When the constraint template is changed, get the default values from the template and overwrite with values from strategy
    
    setConstraintParamsOutput(newcombined);
    const isValid = errors.length === 0;
    isValidConstraintsEditor.current = isValid;
    let updatedconstraint = updateConstraintsParams(jsonOutput, isValid);
    if (isDebugLoggingEnabled) console.log("/handleConstraintsCallBackonstraintsCallBack ConstraintsEditor valid", isValid, errors,updatedconstraint,paramsOutput,updatedParamsOutput.current);
    makeCallBackFordUpdatedDefinition(undefined, undefined, undefined, updatedconstraint,undefined,isValid);
  };

  const updateConstraintsParams = (constraintParamsOutput: { [key: string]: unknown }, isValid: boolean) => {
    const const_strategy = prepareUpdateConstraintsParameters(constraintParamsOutput, constraintSchema); // this is the moent the constraintSchema is added in  const_strategy.constraint
    const newConstraint =  _.merge(updatedConstraintParams ,const_strategy.scheduling_constraints_doc);
    setUpdatedConstraintParams(newConstraint);

    if (isValid) {
      if (isDebugLoggingEnabled) console.log("updateConstraintsParams ...Validating against TMSS server with the strategy will come here", const_strategy, validConstraints);
      setValidConstraints(true); // based upon server validating soon
    } else {
      setValidConstraints(false); // Constraints specification is not valid"
    } return newConstraint;
  };

  return (
    <div>
       <div className="space-y-4 rounded-[8px] bg-lightGrey/30 dark:bg-background-panel p-[20px] pb-[10px] mb-[1em] ">
        {props.strategy?.description && stationGroups && (
          <div className="grouping" style={{ display: "block" }}>
            {_.keys(stationGroups).map((stnGroup) => {
              const divkey = stnGroup + "d";
              return (
                <div style={{ minWidth: "50%", display: "block" }} key={divkey}>
                  <Stations
                    commonSchemaTemplate={props.commonSchemaTemplate}
                    taskName={stnGroup}
                    stationGroup={stationGroups[stnGroup]}
                    height={"auto"}
                    key={stnGroup}
                    onUpdateStations={onUpdateStations}
                    view={props.disable}
                  />
                </div>
              );
            })}
          </div>
        )}
      </div>
      <div className="flex">
        <div className="w-1/2 mr-4">
        
          {paramsSchema && (
            <Jeditor
              key={"SchedulingTasks"}
              title={"Task Parameter"}
              observationType={observationType}
              bandPassFilter={bandPassFilter}
              schema={paramsSchema}
              callback={(jsonputput,errors) => {handleTaskCallBack(jsonputput,errors)}}
              initValue={paramsOutput}
              Theme={theme}
              formatOutput={true}
              showInGrid={false}
              disabled={props.disable}
              isDebugLoggingEnabled={isDebugLoggingEnabled}
            ></Jeditor>
          )}
        </div>
        { 
        <div className="w-1/2  ml-4">
          { (constraintSchema && updatedConstraintParams) &&(
            <SchedulingConstraints
             key= {"SchedulingConstraints"} 
              constraintTemplate={constraintSchema}
              GetSystemUTC={getTime}
              parentFunction={editorFunction}
              callback={(jsonputput,errors) =>{handleConstraintsCallBack(jsonputput,errors)}} 
              Theme={theme}
              initValue={updatedConstraintParams}
              showInGrid={false}
              isDebugLoggingEnabled={isDebugLoggingEnabled}
              formatOutput={true}
              allwaysSetInitOnChange={true}
              disable={props.disable}
            ></SchedulingConstraints>
          )}
        </div>
                     }
      </div>
    </div>
  );
};

export default SpecificationDetailsForm;

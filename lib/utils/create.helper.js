export function getTaskTemplateForTask(taskTemplates, task) {
    let foundTaskTemplates = taskTemplates.filter(t => t.name === task?.specifications_template?.name)
    if (foundTaskTemplates[0] && foundTaskTemplates.length > 1) {
        foundTaskTemplates = foundTaskTemplates.filter(t => t.version === task?.specifications_template?.version)
    }
    if (!foundTaskTemplates[0]) {
        throw new TypeError("Cannot load task for SU since there is no task template found for task '"
            + task?.specifications_template?.name + "' version: " + task?.specifications_template?.version)
    }
    return foundTaskTemplates[0];
}

export function getConstraintTemplate(constraintTemples, name , version ) {
    let foundConstraintTemplate;

    if (name && version) {
        for (let constraint of constraintTemples) {
            if (constraint.name === name && constraint.version === version) {
                foundConstraintTemplate = constraint
                break;
            }
        }
    }

    if (!foundConstraintTemplate) {
        if (!constraintTemples[0]) {
            throw new TypeError("A constraint template should've been set but has not.")
        }
        foundConstraintTemplate = constraintTemples[0]
    }

    return foundConstraintTemplate
}




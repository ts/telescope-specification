import _ from "lodash";
//import $RefParser from "@apidevtools/json-schema-ref-parser";

const ParserUtility = {
  /**
   * Function to get the property of the parameter referred for the task.
   * @param {*} $strategyRefs
   * @param {Array} paramPaths - Property reference path.
   * Example if the task parameter refers '#/tasks/Target Pointing 1/specifications_doc/SAPs/0/digital_pointing',
   * then the parameter to be passed is [digital_pointing, 0, SAPs]
   * @param {Object} taskTemplateSchema - JSON schema for the respective task template
   * @returns
   */
  getParamProperty: async (__$strategyRefs, paramPaths, taskTemplateSchema, taskFilters) => {
    return ParserUtility.getParamPropertyV2( paramPaths, taskTemplateSchema, taskFilters);

  },

  /**
   * To get the default value for parameters if there is no default defined based on the property type
   * @param {string} paramType
   * @returns
   */
  getDefaultParamValue: (paramType) => {
    switch (paramType) {
      case "string": {
        return "";
      }
      case "object": {
        return {};
      }
      case "array": {
        return [];
      }
      case "boolean": {
        return false;
      }
      case "integer": {
        return 0;
      }
      case "number": {
        return 0;
      }
      default: {
        return "";
      }
    }
  },

  getValueFromJsonPointer: (json, pointer) => {
    if (!pointer) return undefined;

    // Convert JSON pointer to Lodash path
    const path = pointer
      .replace(/^#\//, '')  // Remove the starting #/
      .split('/')          // Split by /
      .map(segment => segment.replace(/~1/g, '/').replace(/~0/g, '~'))  // Replace escaped characters
  
    const value = _.get(json, path);
    return value;
  },
  setValueFromJsonPointer: (json, pointer,value) => {
    if (!pointer) return undefined;

    // Convert JSON pointer to Lodash path
    const path = pointer
      .replace(/^#\//, '')  // Remove the starting #/
      .split('/')          // Split by /
      .map(segment => segment.replace(/~1/g, '/').replace(/~0/g, '~'))  // Replace escaped characters
  
     _.set(json, path,value);
     return json;
  },

  /**
   * Function to get the property of the parameter referred for the task.
   * @param {Array} paramPaths - Property reference path.
   * Example if the task parameter refers '#/tasks/Target Pointing 1/specifications_doc/SAPs/0/digital_pointing',
   * then the parameter to be passed is [digital_pointing, 0, SAPs]
   * @param {Object} taskTemplateSchema - JSON schema for the respective task template
   * @param {Any} taskFilters - Filters applied to tasks
   * @returns The resolved parameter property
   */
  getParamPropertyV2: async (paramPaths, taskTemplateSchema, taskFilters) => {

    
    
    let pathIndex = 0;
    let paramProp = {};

    for (const paramPath of paramPaths) {
      let property = taskTemplateSchema.properties[paramPath];
      if (!property) {
        if (taskFilters?.data?.actions?.POST) {
          property = taskFilters.data.actions.POST[paramPath];
        } else {
          property = { type: "string" };
        }
      }

      if (property) {
        let rootPath = paramPaths.slice(0, pathIndex);
        rootPath.reverse();
        paramProp = _.cloneDeep(property);

        if (rootPath.length > 0) {
          for (const path of rootPath) {
            if (paramProp[path]) {
              break;
            } else {
              if (paramProp["$ref"]) {
                paramProp = ParserUtility.getValueFromJsonPointer(taskTemplateSchema, paramProp["$ref"]);
                if (paramProp.properties?.[path]) {
                  paramProp = paramProp.properties[path];
                }
              } else {
                if (paramProp.type === "array" && paramProp.items.properties && paramProp.items.properties[path]) {
                  paramProp = paramProp.items.properties[path];
                } else if (paramProp.type === "array" && paramProp.items["$ref"]) {
                  paramProp = ParserUtility.getValueFromJsonPointer(taskTemplateSchema, paramProp.items["$ref"]);
                  if (paramProp.properties?.[path]) {
                    paramProp = paramProp.properties[path];
                  }
                } else if (paramProp.type === "object" && paramProp.properties && paramProp.properties[path]) {
                  paramProp = paramProp.properties[path];
                }
              }
            }
          }
        }
      }
      pathIndex++;
    }

    return paramProp;
  },

  /**
   * Inject Station parameter if not defined in the strategy parameter
   * @param {*} observStrategy
   */
  addStationParameters(observStrategy) {
    let isUpdated = false;
    if (observStrategy?.template) {
      const parameters = observStrategy.template.parameters;
      let stationParameters = parameters.filter((parameter) => _.endsWith(parameter["refs"], "station_groups"));
      if (stationParameters === undefined || stationParameters.length === 0) {
        const taskKeys = Object.keys(observStrategy.template.tasks);
        for (const key of taskKeys) {
          if (observStrategy?.template.tasks[key]["specifications_doc"]?.["station_configuration"]?.["station_groups"]) {
            observStrategy.template.parameters.push({
              name: "Station Groups - " + key,
              refs: [`#/tasks/${key}/specifications_doc/station_configuration/station_groups`],
            });
            isUpdated = true;
          }
        }
      }
    }
    return isUpdated;
  },
  /**
   * Reorders the schema properties and collapse the last two properties if required
   * @param {Objec} schema - schema JSON object
   * @param {boolean} collapsed - to collpse the last two properties or not
   * @returns Object - updated schema
   */
  updateReservationPropsOrder(schema, collapsed) {
    const propOrders = ["resources", "schedulability", "activity", "effects"];
    for (const propOrder of propOrders) {
      const propIndex = propOrders.indexOf(propOrder);
      schema.properties[propOrder].propertyOrder = propIndex + 1;
      if (propIndex > 1) {
        schema.properties[propOrder].options = { collapsed: collapsed };
      }
    }
    return schema;
  },
   extractdefaultsFromConstraint  (schema) {
    const processSchema = (properties, defaults = {}) => {
      const result = {};
  
      for (const [key, value] of Object.entries(properties)) {
        if (value.type === "object" && value.properties) {
          // If the property is an object, recursively process it
          result[key] = processSchema(value.properties, value.default || {});
        } else if (value.type === "array") {
          // If the property is an array, use the default or an empty array
          result[key] = value.default || [];
        } else {
          // For other types, use the default or fall back to defaults
          result[key] = key in defaults ? defaults[key] : (value.default !== undefined ? value.default :undefined);
        }
      }
  
      return { ...defaults, ...result }; // Combine defaults and result
    };
  
    // Start processing with the top-level properties
    const transformed = processSchema(schema.properties, schema.default || {});
    return transformed;
  },
  reparseParameters(observerStrategy, selectedStationsOfTask, missingStationsofTask, customStationsOfTask, paramsOutput) {
    let observStrategy = _.cloneDeep(observerStrategy);

    if (!observStrategy?.template) return observStrategy;
    // Iterate over each parameter in the template
    for (let index = 0; index < observStrategy.template.parameters.length; index++) {
      let param = observStrategy.template.parameters[index];

      // Iterate over each reference in the parameter
      for (let ref of param.refs) {
        let station_groups = [];
        let hasUpdates = false;
        // Check if the reference includes "station_groups"
        if (ref.includes("station_groups")) {
          // Handle selected stations
          if (selectedStationsOfTask?.[param.name]) {
            for (let selectedGroup of selectedStationsOfTask[param.name]) {
              let stnGroup = missingStationsofTask?.[param.name][selectedGroup];
              if (stnGroup) {
                hasUpdates= true;
              station_groups.push({
                stations: stnGroup.stations,
                max_nr_missing: parseInt(stnGroup.max_nr_missing),
              });
            }
            }
          }

          // Handle custom stations
          if (customStationsOfTask?.[param.name]) {
            
            for (let customGroup of customStationsOfTask[param.name]) {
              hasUpdates= true;
              station_groups.push({
                stations: customGroup.stations,
                max_nr_missing: parseInt(customGroup.max_nr_missing),
              });
            }
          }
          if (hasUpdates ) {
          // Manually set the resolved reference in the parameter
          observStrategy = ParserUtility.setValueFromJsonPointer(observStrategy, ref, station_groups)
          }
          
        } else {
          // Manually set the resolved reference for other types using getValueFromJsonPointer
          const resolvedValue =  paramsOutput?.[param.name]; // ParserUtility.getValueFromJsonPointer(paramsOutput, ref);
          observStrategy = ParserUtility.setValueFromJsonPointer(observStrategy, ref, resolvedValue);

        }
      }
    }
    return observStrategy; 
  }
}




export default ParserUtility;

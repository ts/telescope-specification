import _, {round} from 'lodash';
import moment from "moment";

// Note: While a binary factor is more commonly used for sizes, the correct binary prefix TiB is apparently confusing
// and our stakeholders expressed the wish to see things in TB, so we should use the implied SI prefix factor. Hence
// we should use the SIPrefixFactor throughout the frontend.
export const SIPrefixFactor = 1000;
export const binaryPrefixFactor = 1024;

 const UnitConverter = {

    resourceUnitMap: {
        'time': {display: 'Hours', conversionFactor: 3600, mode: 'decimal', minFractionDigits: 0, maxFractionDigits: 2},
        'bytes': {
            display: 'TB',
            conversionFactor: (SIPrefixFactor ** 4),
            mode: 'decimal',
            minFractionDigits: 0,
            maxFractionDigits: 3
        },
        'bytes_gb': {
            display: 'GB',
            conversionFactor: (SIPrefixFactor ** 3),
            mode: 'decimal',
            minFractionDigits: 0,
            maxFractionDigits: 3
        },
        'gb': {display: 'TB', conversionFactor: (SIPrefixFactor), mode: 'decimal', minFractionDigits: 0, maxFractionDigits: 3},
        'number': {
            display: 'Numbers',
            conversionFactor: 1,
            mode: 'decimal',
            minFractionDigits: 0,
            maxFractionDigits: 0
        },
        'days': {
            display: 'Days',
            conversionFactor: (3600 * 24),
            mode: 'decimal',
            minFractionDigits: 0,
            maxFractionDigits: 0
        }
    },

    getDBResourceUnit: function () {

    },

    getUIResourceUnit: function (type, value) {
        try {
            if (this.resourceUnitMap[type]) {
                let retval = Number.parseFloat(value / (this.resourceUnitMap[type].conversionFactor)).toFixed(this.resourceUnitMap[type].maxFractionDigits)
                return retval;
            }

        } catch (error) {
            console.error('[unit.converter.getUIResourceUnit]', error);
        }
        return value;
    },

    /**
     * Function to convert the duration to string format (Days HH:mm:ss).
     * The days part is of 3 characters prefixed with 0s if it is less than 3 characters.
     * @param {Number} duration - Duration in seconds
     * @returns String - Formatted to 'Day HH:mm:ss' format.
     */
    getSecsToDDHHmmss: function (duration) {
        let days = Math.floor(duration / 86400);
        duration -= days * 86400;

        return `${(days + "").padStart(3, 0)} ${this.getSecsToHHmmss(duration)}`;
    },

    /**
     * Function to get a date (to date) offset from another date(from date) by certain duration.
     * The duration is defined in 'Days Hours:minutes:seconds' format.
     * @param {String} startdate - string of a date object
     * @param {String} duration - duration in string format 'DDD HH:mm:ss'
     * @returns
     */
    getEndDateFromDuration: function (startdate, duration) {
        let values = duration.split(' ');
        let days = values[0];
        let tempStart = moment(startdate);
        let tempEnd = _.clone(tempStart);
        tempEnd.add(days, 'days');
        tempEnd.add(this.getHHmmssToSecs(values[1]), 'seconds');
        return tempEnd.toDate();
    },
    getDuration: function (startdate, stopdate) {
        let startTime = moment(startdate).unix();
        let stopTime = moment(stopdate).unix();
        return UnitConverter.getSecsToDDHHmmss(stopTime - startTime);
    },
    getSecsToHHmmssWithSign: function (seconds) {
        let prefix = '';
        if (!isNaN(seconds)) {
            if (seconds < 0) {
                prefix = '-';
            }
            seconds = prefix + this.getSecsToHHmmss(seconds);
        }
        return seconds;
    },
    getSecsToHrsWithFractionDigits: function (seconds, fractionDigits = 2) {
        if (seconds===null ){ 
            return 0;
        }
        if (typeof seconds !== "number" || isNaN(seconds)) {
            return "0?";
        }
        return (seconds / 3600).toFixed(fractionDigits)
    },
    getSecsToHHmmss: function (seconds) {
        if (!isNaN(seconds)) {
            seconds = Math.abs(seconds);
            const hh = Math.floor(seconds / 3600);
            const mm = Math.floor((seconds - hh * 3600) / 60);
            const ss = +((seconds - (hh * 3600) - (mm * 60)) / 1);
            let retStr = (hh < 10 ? `0${hh}` : `${hh}`) + ':' + (mm < 10 ? `0${mm}` : `${mm}`) + ':' + (ss < 10 ? `0${ss}` : `${ss}`);
            return retStr;
        }
        return seconds;
    },
    getHHmmssToSecs: function (time) {
        if (time) {
            time = _.trim(time);
            let prefix = _.startsWith(time, '-') ? -1 : 1;
            time = time.replace("-", "").replace("+", "");
            const strSeconds = _.split(time, ":");
            if (strSeconds.length === 3) {
                return prefix * (strSeconds[0] * 3600 + strSeconds[1] * 60 + Number(strSeconds[2]));
            } else if (!isNaN(strSeconds)) {
                return parseInt(strSeconds);
            }
        }
        return time;
    },
    getDDDHHmmssToSecs: function (duration) {
        if (duration) {
            duration = duration.replaceAll("_", "0");
            let values = duration.split(' ');
            let days = values[0];
            return days * 86400 + UnitConverter.getHHmmssToSecs(values[1]);
        }
        return duration;
    },
    radiansToDegree: function (object) {
        for (let type in object) {
            if (type === 'transit_offset' || type === 'reference_pointing') {
                continue;
            } else if (typeof object[type] === 'object') {
                this.radiansToDegree(object[type]);
            } else {
                object[type] = ((object[type] * 180) / Math.PI).toFixed(2);
            }
        }
    },
    degreeToRadians(object) {
        for (let type in object) {
            if (type === 'transit_offset' || type === 'reference_pointing') {
                continue;
            } else if (typeof object[type] === 'object') {
                this.degreeToRadians(object[type]);
            } else {
                object[type] = object[type] * (Math.PI / 180);
            }
        }
        return object;
    },
    /**
     * Function to convert Angle 1 & 2 input value for UI.
     */
    getAngleInput(prpInput, isDegree) {
        if (!isNaN(prpInput)) {
            const isNegative = prpInput < 0;
            prpInput = prpInput * (isNegative ? -1 : 1);
            const degrees = prpInput * 180 / Math.PI;
            if (isDegree) {
                let dd = Math.floor(prpInput * 180 / Math.PI);
                let mm = Math.floor((degrees - dd) * 60);
                let ss = round((degrees - dd - (mm / 60)) * 3600, 4);
                if (ss > 59.9999) {
                    mm = mm + 1;
                    ss = 0;
                }
                if (mm > 59) {
                    dd = dd + 1;
                    mm = 0;
                }

                return (isNegative ? '-' : '') + (dd < 10 ? `0${dd}` : `${dd}`) + 'd' + (mm < 10 ? `0${mm}` : `${mm}`) + 'm' + (ss < 10 ? `0${ss}` : `${ss}`) + 's';
            } else {
                let hh = Math.floor(degrees / 15);
                let mm = Math.floor((degrees - (hh * 15)) / 15 * 60);
                let ss = round((degrees - (hh * 15) - (mm * 15 / 60)) / 15 * 3600, 4);
                if (ss > 59.9999) {
                    mm = mm + 1;
                    ss = 0;
                }
                if (mm > 59) {
                    hh = hh + 1;
                    mm = 0;
                }
                return (hh < 10 ? `0${hh}` : `${hh}`) + 'h' + (mm < 10 ? `0${mm}` : `${mm}`) + 'm' + (ss < 10 ? `0${ss}` : `${ss}`) + 's';
            }
        } else {
            
            if (prpInput?.length>1)
                {
                //console.log("It's was not a NAN, but lets keep the value " + prpInput );
                return prpInput; // let's return what we have it is is something, rather then getting rid of it!
                }
                //console.log("It's not a NAN" + prpInput + " defaulting")
            return isDegree ? "0d0m0s" : '0h0m0s';
        }
    },

    /**
     * Function to convert Angle 1 & 2 input value for Backend.
     */
    getAngleOutput(prpOutput, isDegree) {
        if (prpOutput) {
            const splitOutput = prpOutput.split(':');
            const seconds = splitOutput[2] ? splitOutput[2].split('.')[0] : splitOutput[2];
            let milliSeconds = prpOutput.split('.')[1] || '0000';
            milliSeconds = milliSeconds.padEnd(4, 0);
            if (isDegree) {
                return ((splitOutput[0] * 1 + splitOutput[1] / 60 + seconds / 3600 + milliSeconds / 36000000) * Math.PI / 180);
            } else {
                return ((splitOutput[0] * 15 + splitOutput[1] / 4 + seconds / 240 + milliSeconds / 2400000) * Math.PI / 180);
            }
        } else {
            return "00:00:00.0000";
        }
    },
    /**
     * Function to check the input type/format based on the matching predeifined regular expression. It can be any of the supported format
     * like dms, hms, degrees, hours, radians. Example values are 10h10m10s, 10h10m10.1234s, 10:10:10 hour, 10:10:10.1234 hours,
     * 10.1234 hours, 15d15m15s, 15d15m15.1515s, 15:15:15 degree, 15:15:15.1515 degrees, 15.1515 degrees. If only number is entered, it will
     * be considered as radians.
     * @param {String} input - value entered in the angle field.
     * @returns String - the format of the input identified. If no format is identified, returns null.
     */
    getAngleInputType(input) {
        input += '';
        if (input.match(/^((-?)|(\+?))((\d0?d(0?0m)(0?0(\.\d{1,4})?s))|(([\d]?\d{0,1})d(([0-5]?\d)m)(([0-5]?\d)(\.\d{1,4})?s)))$/)) {
            return 'dms';
        } else if (input.match(/^((-?)|(\+?))((\d0(.0{1,4})?)|([\d]?\d{1,3})(\.\d{1,4})?) ?d(egree)?s?$/)) {
            return 'degrees';
        } else if (input.match(/^((-?)|(\+?))((\d0?:(00:)(00))|(([\d]\d{1,3}):(([0-5]\d):)(([0-5]\d)(\.\d{1,4})?))) ?d(egree)?s?$/)) {
            return 'deg_format';
        } else if (input.match(/^(\+?)([0-1]?\d|2[0-3])h([0-5]?\d)m([0-5]?\d)(\.\d{1,4})?s$/)) {
            return 'hms';
        } else if (input.match(/^(\+?)([0-1]?\d|2[0-3])(\.\d{1,4})? ?h(our)?s?$/)) {
            return 'hours';
        } else if (input.match(/^(\+?)([0-1]?\d|2[0-3]):([0-5]?\d):([0-5]?\d)(\.\d{1,4})? ?h(our)?s?$/)) {
            return 'hour_format';
        } else if (input.match(/^((-?)|(\+?))[0-6](\.\d{1,20})?$/)) {
            return 'radians';
        } else {
            return null;
        }
    },
    /**
     * Function to validate an angle input value based on  the format entered and converrt to radians
     * @param {String} angle - value to be parsed to radians.
     * @returns number - radian value.
     */
    parseAngle(angle) {
        let radians = 0;
        const angleType = this.getAngleInputType(angle);
        switch (angleType) {
            case 'dms': {
                radians = this.convertAngleToRadian(angle);
                break;
            }
            case 'hms': {
                radians = this.convertAngleToRadian(angle);
                break;
            }
            case 'degrees': {
                radians = this.convertToRadians(angle.replace('d', '').replace('egree', '').replace('s', '').replace(' ', ''));
                break;
            }
            case 'hours': {
                radians = this.convertToRadians(angle.replace('h', '').replace('our', '').replace('s', '').replace(' ', '') * 15);
                break;
            }
            case 'deg_format': {
                radians = this.getAngleOutput(angle.replace('d', '').replace('egree', '').replace('s', '').replace(' ', ''), true);
                break;
            }
            case 'hour_format': {
                radians = this.getAngleOutput(angle.replace('h', '').replace('our', '').replace('s', '').replace(' ', ''), false);
                break;
            }
            case 'radians': {
                radians = parseFloat(angle);
                break;
            }
            default: {
                radians = angle;
                break;
            }
        }
        return radians;
    },
    /**
     * Convert a degree value to radian
     * @param {*} angle
     * @returns
     */
    convertToRadians(angle) {
        return angle * Math.PI / 180;
    },
    /**
     * Converts a formatted string to a radian value
     * @param {String} angle
     * @returns
     */
    convertAngleToRadian(angle) {
        let radian = 0;
        const isDegree = angle.indexOf('d') > 0;
        const degreeHourSplit = isDegree ? angle.split("d") : angle.split("h");
        let degreeHour = degreeHourSplit[0];
        const isNegativeAngle = parseInt(degreeHour) < 0;
        degreeHour = isNegativeAngle ? degreeHour * -1 : degreeHour;
        const minuteSplit = degreeHourSplit[1].split('m');
        const minute = minuteSplit[0];
        const second = minuteSplit[1].replace('s', '');
        if (isDegree) {
            radian = this.convertToRadians((degreeHour * 1 + minute / 60 + (second) / 3600));
            radian = isNegativeAngle ? radian * -1 : radian;
        } else {
            radian = this.convertToRadians((degreeHour * 15 + minute / 4 + second / 240));
            radian = isNegativeAngle ? radian * -1 : radian;
        }
        return radian;
    },
    getStatusList(suFilters) {
        let statusList = [];
        if (suFilters.data.filters['status']) {
            suFilters.data.filters['status'].choices.forEach(choice => {
                statusList.push(choice.value);
            });
        }
        return statusList;
    },

    /**
     * Converts the string input for the subband list to an Array where human friendly input is accepted
     * Examples: "1,2" -> [1,2]. "1..3" -> [1,2,3]. "1,8..10,15" -> [1,8,9,10,15].
     * @param {String} subbandStringInput
     */
    getSubbandOutput(subbandStringInput) {
        const splittedSubbandString = subbandStringInput ? subbandStringInput.split(",") : [];
        let subbandOutputList = [];
        for (const subbandStringSection of splittedSubbandString) {
            const subbandRangeString = subbandStringSection.split('..');
            if (subbandRangeString.length > 1) { // a range is found
                subbandOutputList.push(...retrieveRangeFromString(subbandRangeString))
            } else { //a single number or something invalid
                const numberValue = parseInt(subbandStringSection)
                if (!isNaN(numberValue)) {
                    subbandOutputList.push(numberValue)
                }
            }
        }
        return subbandOutputList;
    }
};

function range(start, end) {
    if (start === end) return [start];
    return [start, ...range(start + 1, end)];
}

function retrieveRangeFromString(subbandRange) {
    const start = parseInt(subbandRange[0])
    const end = parseInt(subbandRange[1])
    if (!isNaN(start) && !isNaN(end)) {
        return range(start, end)
    }
    return []
}

export default UnitConverter
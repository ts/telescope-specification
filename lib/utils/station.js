import _ from "lodash";
const Station = {

getStationGroup:  function (commonSchemaTemplate) {
    try {
        let stationGroupName = [];
        const stationGroups = commonSchemaTemplate.ref_resolved_schema.definitions.station_group.anyOf;
        for (const obj of stationGroups) {
            if (obj.title !== 'Custom') {
                stationGroupName.push({value: obj.title});
            }
        }
        return stationGroupName;
    } catch (error) {
        console.error(error);
        return [];
    }
},
getStations:  function (commonSchemaTemplate,group) {
    let stationGroup = {};
    try {
        const stationGroups = commonSchemaTemplate.ref_resolved_schema?.definitions?.station_group.anyOf;
        const stationGroupObj = _.find(stationGroups, {title: group});
        if (stationGroupObj) {
            stationGroup = stationGroupObj?.default;
        }
    } catch (error) {
        console.error(error);
        return [];
    }
    stationGroup['group'] = group;
    return stationGroup;
},

ValidateStations: function (stationGroups,selectedStationsOfTask,customStationsOfTask,missingStationsofTask) {
    /*
     * Checks if at least one station selected for each station_group parameter and max_nr_missing station value is entered.
     * @returns Boolean
     */
    
      let validStations = false;
      // Check if at least one station selected for all station groups(parameters)
      // Break the loop if any of the required condition check is not satisfied
      for (const stationGroup of _.keys(stationGroups)) {
        // Check if the selected stations and custom stations are not empty
        if (  selectedStationsOfTask[stationGroup] &&  customStationsOfTask[stationGroup]  ) {
          // Get the stations list for the station group(parameter)
          const stationsList = selectedStationsOfTask[stationGroup].concat(customStationsOfTask[stationGroup]);
          //If stationsList is empty then no station group and custom stations are selected
          if (stationsList.length === 0) {
            validStations = false;
            break;
          } else {
            //If predefined station group(Dutch, Remote, etc.,) is/are selected, validate if max_nr_missing is entered
            if (selectedStationsOfTask[stationGroup].length > 0) {
              for (const selectedStations of selectedStationsOfTask[
                stationGroup
              ]) {
                const selectedGroup = missingStationsofTask[stationGroup][selectedStations];
                if (selectedGroup) {
                  if (selectedGroup.max_nr_missing === "") {
                    validStations = false;
                    break;
                  } else {
                    validStations = true;
                  }
                }
              }
              if (!validStations) {
                break;
              } else {
                validStations = true;
              }
            }
            //If custom station group(s) is/are selected, validate if max_nr_missing is entered
            if (customStationsOfTask[stationGroup].length > 0) {
              for (const customStations of customStationsOfTask[
                stationGroup
              ]) {
                if (customStations.max_nr_missing === "") {
                  validStations = false;
                  break;
                } else {
                  validStations = true;
                }
              }
              if (!validStations) {
                break;
              } else {
                validStations = true;
              }
            }
          }
        } else {
          validStations = false;
          break;
        }
      }
      return validStations;
    }
}
export default Station
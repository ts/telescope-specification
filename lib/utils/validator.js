import  UnitConverter  from "./unit.converter";
import moment from "moment";
/* Note this is legacy code taken over as-is, we should refactor it*/
 const Validator = {
    validateTime(value) {
        if (!value) {
            return false;
        }
        const angleType = UnitConverter.getAngleInputType(value);
        if (angleType && ['hms', 'hours', 'hour_format', 'radians'].indexOf(angleType) >= 0) {
            if (angleType === 'radians' && (parseFloat(value) < 0 || parseFloat(value) > 6.2831)) {
                return false;
            }
            return true;
        }
        return false;
    },
    validateTimeAndAngle(value) {
        if (!value) {
            return false;
        }
        const angleType = UnitConverter.getAngleInputType(value);
        if (angleType && ['hms', 'hours', 'hour_format', 'radians', 'degrees', 'deg_format', 'dms'].indexOf(angleType) >= 0) {
            if (angleType === 'radians' && (parseFloat(value) < 0 || parseFloat(value) > 6.2831)) {
                return false;
            }
            if (angleType === 'dms') {
                let radianValue = UnitConverter.convertAngleToRadian(value)
                if (radianValue < 0 || radianValue > 6.283185306694774) {
                    return false
                }
            }
            if (angleType === 'degrees') {
                let radianValue = UnitConverter.convertToRadians(value.split("d")[0])
                if (radianValue < 0 || radianValue > 6.283185306694774) {
                    return false
                }
            }
            if (angleType === 'deg_format') {
                let radianValue = UnitConverter.getAngleOutput(value.replace('d', '').replace('egree', '').replace('s', '').replace(' ', ''), true);
                if (radianValue < 0 || radianValue > 6.283185306694774) {
                    return false
                }
            }
            return true;
        }
        return false;
    },
    validateAngle(value) {
        if (!value) {
            return false;
        }

        const angleType = UnitConverter.getAngleInputType(value);
        if ( angleType && ['dms', 'degrees', 'deg_format', 'radians'].indexOf(angleType) >= 0) {
            let radianValue = -9999
            if (angleType === 'radians') {
                radianValue = parseFloat(value)
            } else  if (angleType === 'dms') {
                 radianValue = UnitConverter.convertAngleToRadian(value)
            } else if (angleType === 'degrees') {
                 radianValue = UnitConverter.convertToRadians(value.split("d")[0])
            } else if (angleType === 'deg_format') {
                 radianValue = UnitConverter.getAngleOutput(value.replace('d', '').replace('egree', '').replace('s', '').replace(' ', ''), true)
            }
            if (radianValue < -1.5707963268 || radianValue > 1.5707963268) {
                return false
            }

            return true;
        }
        return false;
    },

    /**
     * Validates whether any of the given property values is modified comparing the old and new object.
     * @param {Object} oldObject - old object that is already existing in the state list
     * @param {Object} newObject - new object received from the websocket message
     * @param {Array} properties - array of string (name of the properties) to veriy
     */
    isObjectModified(oldObject, newObject, properties) {
        let isModified = false;
        // If oldObject is not found, the object should be got from server
        if (!oldObject && newObject) {
            return true;
        }
        for (const property of properties) {
            if (oldObject[property] !== newObject[property]) {
                isModified = true;
            }
        }
        return isModified;
    },
    /**
     * Validate time
     * @param {string} time; HH:mm:ss format
     * * @param {boolean} excludeHour the first hour number excluded (i.e. 01:23:45 is valid as 1:23:45)
     * @returns {boolean}
     */
    isValidHHmmss(time, excludeHour = true) {
        const momentFormats = ["HH:mm:ss"]
        if (excludeHour) {
            momentFormats.push("H:mm:ss")
        }
        const momentTime = new moment(time, momentFormats, true)
        return momentTime.isValid()
    },
    /**
     * Validate duration in time
     * @param duration in HH:mm:ss format
     * @return {boolean}
     */
    isValidDuration(duration) {
        const timeFormat = /^([0-9]+):([0-5]?[0-9]):([0-5]?[0-9])$/;

        return timeFormat.test(duration);
    },
    /**
     * Validate Day time
     * @param {string value with DDD HH:mm:ss format} Days time 
     * @returns 
     */
    isValidDDDHHmmss(duration, excludeHour) {
        let isValid = true;
        let values = duration.split(' ');
        isValid = Validator.isValidHHmmss(values[1], excludeHour)
        return isValid;
    },

    /**
     *  Check object is empty or not
     * @param {String object}
     * @returns True/False
     */
    isEmpty(value) {
        if (value === undefined) {
            return true;
        }/*   else if (!value) {
            return true;
        }   */else if (value.length === 0) {
            return true;
        } else {
            return false;
        }
    },
    validateTransitOffset(schema, jsonOutput, error, path) {
        const tmpTime = jsonOutput?.split?.(":");
        if ((!jsonOutput || jsonOutput.length === 0) && schema.required === true) {
            error.push({
                message: `Transit Offset - ${schema.title} is required. Time format should be [+/- Hours:Minutes:Seconds]. eg. '-23:59:59', '+20:23:25', '15:45:45'`,
                path: path,
                property: 'validationType',
            });
        }
        //here the isValidHHmmss() valiadtion function not used because it requires only MM:SS validation and the hours may define more than 23
        else 
        if (tmpTime) {
        if (tmpTime.length !== 3 || tmpTime[1] > 59 || tmpTime[1].trim() === '' || tmpTime[2] > 59 || tmpTime[2].trim() === '') {
            error.push({
                message: "Invalid time format. Time format should be [+/- Hours:Minutes:Seconds]. eg. '-23:59:59', '+20:23:25', '15:45:45'",
                path: path,
                property: 'validationType',
            });
        } } else {
            let value = UnitConverter.getHHmmssToSecs(jsonOutput);
            if (isNaN(value) || (value < schema.minimum || value > schema.maximum)) {
                error.push({
                    message: 'Time must be between ' + ((schema.minimum < 0) ? '-' : '') + UnitConverter.getSecsToHHmmss(schema.minimum) + ' and ' + ((schema.maximum < 0) ? '-' : '') + UnitConverter.getSecsToHHmmss(schema.maximum),
                    path: path,
                    property: 'validationType',
                });
            }
        }
    },
    validateDurationHHmmss(schema, jsonOutput, error, path) {
        if (!this.isValidDuration(jsonOutput)) {
            error.push({
                message: "Invalid duration. Duration should be in 'HH:mm:ss' format and between " + UnitConverter.getSecsToHHmmss(schema.minimum) + ' and ' + UnitConverter.getSecsToHHmmss(schema.maximum),
                path: path,
                property: 'validationType',
            });
        } else {
            let value = UnitConverter.getHHmmssToSecs(jsonOutput);
            if (isNaN(value) || (value < schema.minimum || value > schema.maximum)) {
                error.push({
                    message: 'Duration must be between ' + UnitConverter.getSecsToHHmmss(schema.minimum) + ' and ' + UnitConverter.getSecsToHHmmss(schema.maximum),
                    path: path,
                    property: 'validationType',
                });
            }
        }
    },
    /**
     * Validate if the email value is in valid email format
     * @param {String} email - email string 
     * @returns boolean - true if email value matches the regex pattern else false
     */
    isValidEmail(email) {
        const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailRegex.test(email.toLowerCase());
    }
};


export default Validator
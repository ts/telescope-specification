import UnitConverter from "./unit.converter";
import moment from "moment";
import _ from 'lodash';
export function prepareUpdateConstraintsParameters(constraintParamsOutput,constraintSchema) {
    const constStrategy = _.cloneDeep(constraintParamsOutput); 
    if (constStrategy)
      for (let type in constStrategy.time) {
        if (constStrategy.scheduler === "online" ||
          constStrategy.scheduler === "dynamic") {
          // delete constStrategy.time.at;
        }
        if (!constStrategy.time.after) {
          delete constStrategy.time.after;
        }
        if (!constStrategy.time.before) {
          delete constStrategy.time.before;
        }
        if (constStrategy.time[type] && constStrategy.time[type].length > 0) {
          if (typeof constStrategy.time[type] === "string") {
            constStrategy.time[type] = `${moment(
              constStrategy.time[type]
            ).format("YYYY-MM-DDTHH:mm:ss.SSSSS", { trim: false })}Z`;
          } else {
            constStrategy.time[type].forEach((time) => {
              for (let key in time) {
                time[key] = `${moment(time[key]).format(
                  "YYYY-MM-DDTHH:mm:ss.SSSSS",
                  { trim: false }
                )}Z`;
              }
            });
          }
        }
      }
    if (constStrategy?.sky?.transit_offset) {
     // console.log("Current constStrategy?.sky before ",constStrategy?.sky)
      constStrategy.sky.transit_offset.from = UnitConverter.getHHmmssToSecs(
        constStrategy.sky.transit_offset.from
      );
      constStrategy.sky.transit_offset.to = UnitConverter.getHHmmssToSecs(
        constStrategy.sky.transit_offset.to
      );
    }
    //console.log("Current constStrategy?.sky after",constStrategy?.sky)
    UnitConverter.degreeToRadians(constStrategy?.sky);
    const const_strategy = {
      scheduling_constraints_doc: constStrategy,
      id: constraintSchema.id,
      constraint: _.cloneDeep(constraintSchema),
    };
    return const_strategy;
  }


 export function recombineStrategy(modifiedObservStrategy,selectedStationsOfTask,missingStationsofTask,customStationsOfTask,paramsOutput) {
    let observStrategy = _.cloneDeep(modifiedObservStrategy); 
    
    if (observStrategy?.template) {
    observStrategy.template.parameters.forEach((param, index) => {
      let refIndex = 0;
      for (const ref of param.refs) {
        let station_groups = [];
        if (ref?.includes?.("station_groups")) {
          if (selectedStationsOfTask[param.name]) {
            for (const selectedGroup of selectedStationsOfTask[param.name]) {
              const stnGroup = missingStationsofTask[param.name][selectedGroup];
              station_groups.push({
                stations: stnGroup.stations,
                max_nr_missing: parseInt(stnGroup.max_nr_missing),
              });
            }
          }
  
          if (customStationsOfTask[param.name]) {
            for (const customGroup of customStationsOfTask[param.name]) {
              station_groups.push({
                stations: customGroup.stations,
                max_nr_missing: parseInt(customGroup.max_nr_missing),
              });
            }
          }
          observStrategy.template.parameters[index].refs[refIndex] = station_groups;
        } else {
          observStrategy.template.parameters[index].refs[refIndex] = paramsOutput[param.name];
        }
        refIndex++;
      }
    }
    );
    }
    return observStrategy;
  }
  
import Validator from "../utils/validator"
import { describe,test,expect } from "vitest";
describe('isValidHHmmss', () => {
    test.each(['12:34:56', '23:00:59', '0:59:56', '1:01:59'])('Valid time: %s', (time) => {
        expect(Validator.isValidHHmmss(time)).toBe(true);
    })

    test.each(['25:34:56', '12:60:30', '12:34:67', '25:34', 'invalid', '78:56', '12:67', '12:', '12'])('Invalid time: %s', (time) => {
        expect(Validator.isValidHHmmss(time)).toBe(false);
    })

    test.each(['0:59:56', '1:01:59'])('Invalid time when hour must be included: %s', (time) => {
        expect(Validator.isValidHHmmss(time, false)).toBe(false);
    })

    test.each(['', undefined])('Missing input: %s', (time) => {
        expect(Validator.isValidHHmmss(time)).toBe(false);
    })
});

describe('isValidDuration', () => {
    test.each(['00:00:00', '12:34:56', '01:02:03', '24:00:00', '50:00:00', '12345:00:00'])('Valid durations: %s', (time) => {
        expect(Validator.isValidDuration(time)).toBe(true);
    })

    test.each(['00:61:00', '00:00:61', '12:34', 'invalid'])('Invalid durations: %s', (time) => {
        expect(Validator.isValidDuration(time)).toBe(false);
    })

    test.each(['', undefined])('Missing input: %s', (time) => {
        expect(Validator.isValidDuration(time)).toBe(false);
    })

    test.each(['12-34-56', '12.34.56', '12/34/56', '12_34_56'])('Input in wrong format: %s', (time) => {
        expect(Validator.isValidDuration(time)).toBe(false);
    })
});


describe('validateTimeAndAngle' ,() => {
    test.each([null,"-1 hours","900 degrees" ])('Invalid Time and angle: %s', (timeandangle) => {

        expect(Validator.validateTimeAndAngle(timeandangle)).toBe(false);
    })


    test.each(["2 hours", "5 degrees" ])('valid Time and angle: %s', (timeandangle) => {

        expect(Validator.validateTimeAndAngle(timeandangle)).toBe(true);
    })
});
import {
  getConstraintTemplate,
  getTaskTemplateForTask,
} from "../utils/create.helper";
import { describe, test, expect } from "vitest";
describe("getConstraintTemplate", () => {
  const constraintTemplateOne = {
    name: "x-men",
    version: "8",
    heroes: ["x-men", "cyclops"],
  };
  const name = "batman";
  const version = "1";
  const constraintTemplateTwo = {
    name: name,
    version: version,
    heroes: ["batman", "robin"],
  };

  test("first from props when no name+version", () => {
    const constraintTemplates = [constraintTemplateOne, constraintTemplateTwo];
    const actualConstraintTemplate = getConstraintTemplate(constraintTemplates);
    expect(actualConstraintTemplate).toBe(constraintTemplateOne);
  });
  test("first from props when name+version not found", () => {
    const constraintTemplates = [constraintTemplateOne, constraintTemplateTwo];
    const actualConstraintTemplate = getConstraintTemplate(
      constraintTemplates,
      "does-not",
      "exist"
    );
    expect(actualConstraintTemplate).toBe(constraintTemplateOne);
  });
  test("with name and version", () => {
    const constraintTemplates = [constraintTemplateOne, constraintTemplateTwo];
    const actualConstraintTemplate = getConstraintTemplate(
      constraintTemplates,
      name,
      version
    );
    expect(actualConstraintTemplate).toBe(constraintTemplateTwo);
  });
  test("fail when no templates are set", () => {
    expect(() => {
      getConstraintTemplate([]);
    }).toThrow(TypeError);
  });
});

describe("getTaskTemplateForTask", () => {
  const taskTemplateOne = {
    name: "x-men",
    version: "8",
    heroes: ["x-men", "cyclops"],
  };
  const name = "batman";
  const version = "1";
  const taskTemplateTwo = {
    name: name,
    version: 1337,
    heroes: ["batman", "robin"],
  };
  const taskTemplateThree = {
    name: name,
    version: version,
    heroes: ["batman", "robin"],
  };
  const taskTemplateFour = {
    name: "simba",
    version: version,
    heroes: ["batman", "robin"],
  };

  test("found single template on name", () => {
    const task = { specifications_template: { name: name, version: version } };
    const taskTemplates = [taskTemplateOne, taskTemplateThree];
    const actualTaskTemplate = getTaskTemplateForTask(taskTemplates, task);
    expect(actualTaskTemplate).toBe(taskTemplateThree);
  });
  test("found multiple on name, single on version", () => {
    const task = { specifications_template: { name: name, version: version } };
    const taskTemplates = [
      taskTemplateFour,
      taskTemplateTwo,
      taskTemplateThree,
    ];
    const actualTaskTemplate = getTaskTemplateForTask(taskTemplates, task);
    expect(actualTaskTemplate).toBe(taskTemplateThree);
  });
  test("fail when no templates are found on name", () => {
    const task = {
      specifications_template: { name: "does-not-exist", version: version },
    };
    const taskTemplates = [taskTemplateOne, taskTemplateTwo, taskTemplateThree];
    expect(() => {
      getTaskTemplateForTask(taskTemplates, task);
    }).toThrow(TypeError);
  });
  test("fail when multiple template found on name but none on version", () => {
    const task = {
      specifications_template: { name: name, version: "does-not-exist" },
    };
    const taskTemplates = [taskTemplateOne, taskTemplateTwo, taskTemplateThree];
    expect(() => {
      getTaskTemplateForTask(taskTemplates, task);
    }).toThrow(TypeError);
  });
  test("fail when no templates are set", () => {
    expect(() => {
      getTaskTemplateForTask([]);
    }).toThrow(TypeError);
  });
  test("fail when no task is specified", () => {
    const taskTemplates = [taskTemplateOne, taskTemplateTwo, taskTemplateThree];
    expect(() => {
      getTaskTemplateForTask(taskTemplates);
    }).toThrow(TypeError);
  });
  test("fail when necessary task parameters for retrieval of template are not set", () => {
    const task = { redundant: { name: name, version: version } };
    const taskTemplates = [taskTemplateOne, taskTemplateTwo, taskTemplateThree];
    expect(() => {
      getTaskTemplateForTask(taskTemplates, task);
    }).toThrow(TypeError);
  });
});

import { describe,test,expect,it } from "vitest";
import  UnitConverter   from "../utils/unit.converter"


describe('getSubbandOutput', () => {
    test.each(['', null, undefined])('should return empty array on not specified input', (subbandStringInput) => {
            const result = UnitConverter.getSubbandOutput(subbandStringInput);
            expect(result).toEqual([]);
        }
    )

    it('should convert a string of subbands to an array', () => {
        const subbandString = '1,2,3,5';
        const result = UnitConverter.getSubbandOutput(subbandString);
        expect(result).toEqual([1, 2, 3, 5]);
    });

    it('should handle subband ranges correctly', () => {
        const subbandString = '1..3,5..7';
        const result = UnitConverter.getSubbandOutput(subbandString);
        expect(result).toEqual([1, 2, 3, 5, 6, 7]);
    });

    it('should handle negative subband ranges correctly', () => {
        const subbandString = '-3..-1';
        const result = UnitConverter.getSubbandOutput(subbandString);
        expect(result).toEqual([-3, -2, -1]);
    });

    test.each(['1..2,,4..5', '1,2,three,4,5', '1,2,4,5,9...18'])('should ignore invalid entries within the subband string', (subbandString) => {
        const result = UnitConverter.getSubbandOutput(subbandString);
        expect(result).toEqual([1, 2, 4, 5]);
    })
});

describe('getSecsToHrsWithFractionDigits', () => {
    it('should convert seconds to hours with default fraction digits', () => {
        const seconds = 7200; // 2 hours

        const result = UnitConverter.getSecsToHrsWithFractionDigits(seconds);

        expect(result).toBe('2.00');
    });

    it('should convert seconds to hours with custom fraction digits', () => {
        const seconds = 3600; // 1 hour
        const fractionDigits = 1;

        const result = UnitConverter.getSecsToHrsWithFractionDigits(seconds, fractionDigits);

        expect(result).toBe('1.0');
    });

    it('should throw Range Error when fraction digits is negative', () => {
        const seconds = 3600; // 1 hour
        const fractionDigits = -1;

        expect(() => {
            UnitConverter.getSecsToHrsWithFractionDigits(seconds, fractionDigits);
        }).toThrow(RangeError);
    });

    it('should handle zero seconds', () => {
        const seconds = 0;
        const result = UnitConverter.getSecsToHrsWithFractionDigits(seconds);
        expect(result).toBe('0.00');
    });

    it('should handle negative seconds', () => {
        const seconds = -1337; //37 minutes

        const result = UnitConverter.getSecsToHrsWithFractionDigits(seconds);

        expect(result).toBe('-0.37');
    });

    test.each(["12",  undefined, "aa"])('Should handle invalid data with 0? for wrong input: %s', (wrongSeconds) => {
        const result = UnitConverter.getSecsToHrsWithFractionDigits(wrongSeconds);
        expect(result).toBe('0?');
    });


    it('should handle null with 0', () => {
        const wrongSeconds = null; 
        const result = UnitConverter.getSecsToHrsWithFractionDigits(wrongSeconds);
        expect(result).toBe(0);
    });
});